"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shows_service_1 = require("../../services/shows.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var show_component_1 = require("../show/show.component");
var global_service_1 = require("../../services/global.service");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var WatchlistComponent = (function () {
    function WatchlistComponent(service, _showsService, _router, responseHandler, title) {
        this.service = service;
        this._showsService = _showsService;
        this._router = _router;
        this.responseHandler = responseHandler;
        this.title = title;
        title.setTitle("Watchlist");
    }
    WatchlistComponent.prototype.ngOnInit = function () {
        if (!this.service.isLoggedIn()) {
            this._router.navigate(['Login']);
            return;
        }
        this.getShows();
    };
    WatchlistComponent.prototype.getShows = function () {
        var _this = this;
        this._showsService.getShows('watchlist')
            .subscribe(function (shows) {
            _this.shows = shows;
            _this.shows.forEach(function (show) { return show.poster = _this.service.getURL() + 'shows/' + show.id + '/poster'; });
        }, function (error) { return _this.handleError(error); });
    };
    WatchlistComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    WatchlistComponent = __decorate([
        core_1.Component({
            selector: 'shows-list',
            templateUrl: 'app/components/watchlist/watchlist.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, show_component_1.ShowComponent],
            providers: [shows_service_1.ShowsService]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, shows_service_1.ShowsService, router_deprecated_1.Router, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], WatchlistComponent);
    return WatchlistComponent;
}());
exports.WatchlistComponent = WatchlistComponent;
//# sourceMappingURL=watchlist.component.js.map