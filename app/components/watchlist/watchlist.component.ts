import {Component, OnInit, ElementRef} from '@angular/core';
import {ShowsService} from "../../services/shows.service";
import {Show} from "../../model/show";
import {Router, RouterLink, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ShowComponent} from "../show/show.component";
import {GlobalService} from "../../services/global.service";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'shows-list',
    templateUrl: 'app/components/watchlist/watchlist.html',
    directives: [ROUTER_DIRECTIVES, ShowComponent],
    providers: [ShowsService]
})
export class WatchlistComponent implements OnInit {
    constructor(
        private service:GlobalService,
        private _showsService:ShowsService,
        private _router:Router,
        private responseHandler:ResponseHandler,
        private title:Title
    ) {
        title.setTitle("Watchlist")
    }

    shows:Show[];

    ngOnInit() {
        if (!this.service.isLoggedIn()) {
            this._router.navigate(['Login']);
            return
        }
        this.getShows();
    }

    getShows() {
        this._showsService.getShows('watchlist')
            .subscribe(
                shows => {
                    this.shows = shows
                    this.shows.forEach(show => show.poster = this.service.getURL() + 'shows/' + show.id + '/poster')
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}