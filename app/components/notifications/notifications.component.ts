import {Component, OnInit, ElementRef} from '@angular/core';
import {Router, RouterLink, ROUTER_DIRECTIVES}        from '@angular/router-deprecated';
import {GlobalService} from "../../services/global.service";
import {ProfileService} from "../../services/profile.service";
import {MyDatePipe} from "../../MyDatePipe";
import {TimeAgoPipe} from "../../TimeAgoPipe";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'notifications',
    templateUrl: 'app/components/notifications/notifications.html',
    pipes: [MyDatePipe, TimeAgoPipe],
    providers: [ProfileService],
    directives: [ROUTER_DIRECTIVES]
})

export class NotificationsComponent implements OnInit {
    constructor(
        private service:GlobalService,
        private router:Router,
        private profileService:ProfileService,
        private responseHandler:ResponseHandler,
        private title:Title){
            this.title.setTitle("Notifications")
    }

    notifications;

    ngOnInit() {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return
        }
        this.getNotifications()
    }

    getNotifications() {
        this.profileService.getNotifications()
            .subscribe(
                data => {
                    this.notifications = data
                    this.notifications.forEach(notification => this.getUser(notification))
                    this.notifications.sort(this.compare)
                },
                error => this.handleError(error)
            );
    }

    compare(a, b) {
        if (a.created_at < b.created_at)
            return 1;
        else if (a.created_at > b.created_at)
            return -1;
        else
            return 0;
    }


    getUser(notification) {
        if(notification.follower_id){
            notification.picture = this.service.getURL() + 'users/' + notification.follower_id + '/picture'
        }
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}