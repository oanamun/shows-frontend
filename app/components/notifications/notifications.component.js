"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("../../services/global.service");
var profile_service_1 = require("../../services/profile.service");
var MyDatePipe_1 = require("../../MyDatePipe");
var TimeAgoPipe_1 = require("../../TimeAgoPipe");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var NotificationsComponent = (function () {
    function NotificationsComponent(service, router, profileService, responseHandler, title) {
        this.service = service;
        this.router = router;
        this.profileService = profileService;
        this.responseHandler = responseHandler;
        this.title = title;
        this.title.setTitle("Notifications");
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        this.getNotifications();
    };
    NotificationsComponent.prototype.getNotifications = function () {
        var _this = this;
        this.profileService.getNotifications()
            .subscribe(function (data) {
            _this.notifications = data;
            _this.notifications.forEach(function (notification) { return _this.getUser(notification); });
            _this.notifications.sort(_this.compare);
        }, function (error) { return _this.handleError(error); });
    };
    NotificationsComponent.prototype.compare = function (a, b) {
        if (a.created_at < b.created_at)
            return 1;
        else if (a.created_at > b.created_at)
            return -1;
        else
            return 0;
    };
    NotificationsComponent.prototype.getUser = function (notification) {
        if (notification.follower_id) {
            notification.picture = this.service.getURL() + 'users/' + notification.follower_id + '/picture';
        }
    };
    NotificationsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    NotificationsComponent = __decorate([
        core_1.Component({
            selector: 'notifications',
            templateUrl: 'app/components/notifications/notifications.html',
            pipes: [MyDatePipe_1.MyDatePipe, TimeAgoPipe_1.TimeAgoPipe],
            providers: [profile_service_1.ProfileService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, router_deprecated_1.Router, profile_service_1.ProfileService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], NotificationsComponent);
    return NotificationsComponent;
}());
exports.NotificationsComponent = NotificationsComponent;
//# sourceMappingURL=notifications.component.js.map