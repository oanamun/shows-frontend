"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shows_service_1 = require("../../services/shows.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var show_component_1 = require("../show/show.component");
var global_service_1 = require("../../services/global.service");
var SMRating_1 = require("../../semantic/SMRating");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var ShowsListComponent = (function () {
    function ShowsListComponent(service, showsService, responseHandler, title) {
        this.service = service;
        this.showsService = showsService;
        this.responseHandler = responseHandler;
        this.title = title;
        title.setTitle("Shows");
    }
    ShowsListComponent.prototype.ngOnInit = function () {
        this.getShows('all');
        this.getCategories();
        this.initDropdown();
    };
    ShowsListComponent.prototype.onFilterChange = function (value) {
        this.getShows(value);
    };
    ShowsListComponent.prototype.onSortChange = function (value) {
        var order = 1;
        if (value == 'rating') {
            order = -1;
        }
        this.shows.sort(function (a, b) {
            if (a[value] > b[value]) {
                return 1 * order;
            }
            else if (a[value] < b[value]) {
                return -1 * order;
            }
            return 0;
        });
    };
    ShowsListComponent.prototype.initDropdown = function () {
        jQuery('.my-dropdown').dropdown();
        jQuery('#rating').rating();
    };
    ShowsListComponent.prototype.getShows = function (type) {
        var _this = this;
        this.showsService.getShows(type)
            .subscribe(function (shows) {
            _this.shows = shows;
            _this.shows.forEach(function (show) { return show.poster = _this.service.getURL() + 'shows/' + show.id + '/poster'; });
        }, function (error) { return _this.handleError(error); });
    };
    ShowsListComponent.prototype.getCategories = function () {
        var _this = this;
        this.showsService.getCategories()
            .subscribe(function (categories) { return _this.categories = categories; }, function (error) { return _this.handleError(error); });
    };
    ShowsListComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An error occured", error);
    };
    ShowsListComponent = __decorate([
        core_1.Component({
            selector: 'shows-list',
            templateUrl: 'app/components/shows_list/shows_list.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, show_component_1.ShowComponent, SMRating_1.SMRating],
            providers: [shows_service_1.ShowsService]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, shows_service_1.ShowsService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], ShowsListComponent);
    return ShowsListComponent;
}());
exports.ShowsListComponent = ShowsListComponent;
//# sourceMappingURL=shows_list.component.js.map