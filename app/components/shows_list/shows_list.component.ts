import {Component, OnInit, ElementRef} from '@angular/core';
import {ShowsService} from "../../services/shows.service";
import {Show} from "../../model/show";
import {RouteParams, RouterLink, Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ShowComponent} from "../show/show.component";
import {GlobalService} from "../../services/global.service";
import {SMRating} from "../../semantic/SMRating";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'shows-list',
    templateUrl: 'app/components/shows_list/shows_list.html',
    directives: [ROUTER_DIRECTIVES, ShowComponent, SMRating],
    providers: [ShowsService]
})
export class ShowsListComponent implements OnInit {
    constructor(
        private service:GlobalService,
        private showsService:ShowsService,
        private responseHandler:ResponseHandler,
        private title:Title) {
            title.setTitle("Shows")
    }

    shows:Show[];
    categories;

    ngOnInit() {
        this.getShows('all');
        this.getCategories();
        this.initDropdown();

    }

    onFilterChange(value) {
        this.getShows(value)
    }

    onSortChange(value) {
        let order =1;
        if(value == 'rating'){
            order= -1;
        }
        this.shows.sort((a, b) => {
            if (a[value] > b[value]) {
                return 1*order;
            }
            else if (a[value] < b[value]) {
                return -1*order;
            }
            return 0;
        })
    }

    initDropdown() {
        (<any>jQuery('.my-dropdown')).dropdown();

        (<any>jQuery('#rating')).rating();
    }

    getShows(type) {
        this.showsService.getShows(type)
            .subscribe(
                shows => {
                    this.shows = shows
                    this.shows.forEach(show => show.poster = this.service.getURL() + 'shows/' + show.id + '/poster')
                },
                error => this.handleError(error)
            );
    }

    getCategories() {
        this.showsService.getCategories()
            .subscribe(
                categories => this.categories = categories,
                error => this.handleError(error)
            );
    }


    handleError(error) {
        this.responseHandler.errorMessage("An error occured", error)
    }
}