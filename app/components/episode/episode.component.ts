import {Component, OnInit}  from '@angular/core';
import {RouteParams, RouterLink, ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {Show} from "../../model/show";
import {GlobalService} from "../../services/global.service";
import {SMRating} from "../../semantic/SMRating";
import {User} from "../../model/user";
import {EpisodesService} from "../../services/episodes.service";
import {WatchersComponent} from "../show/watchers/watchers.component";
import {DiscussionsListComponent} from "./discussions_list/discussions_list.component";
import {MyDatePipe} from "../../MyDatePipe";
import {EpisodeActionsComponent} from "./episode_actions/episode_actions.component.";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'episode',
    templateUrl: 'app/components/episode/episode.html',
    providers: [EpisodesService],
    pipes: [MyDatePipe],
    directives: [ROUTER_DIRECTIVES, SMRating, WatchersComponent, DiscussionsListComponent, EpisodeActionsComponent]
})

export class EpisodeComponent implements OnInit {
    constructor(private episodesService:EpisodesService,
                private routeParams:RouteParams,
                private service:GlobalService,
                private responseHandler:ResponseHandler,
                private title:Title) {}

    episode;
    isLoggedIn;

    ngOnInit() {
        this.getEpisode();
        this.isLoggedIn = this.service.isLoggedIn()
    }

    getEpisode() {
        this.episodesService.getEpisode(this.routeParams.get('id'))
            .subscribe(
                data => {
                    this.episode = data
                    this.episode.poster = this.service.getURL()+"shows/"+this.episode.show_id+"/poster"
                    this.title.setTitle(this.episode.show+" | "+this.episode.name)
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}