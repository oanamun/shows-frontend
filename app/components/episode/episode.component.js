"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("../../services/global.service");
var SMRating_1 = require("../../semantic/SMRating");
var episodes_service_1 = require("../../services/episodes.service");
var watchers_component_1 = require("../show/watchers/watchers.component");
var discussions_list_component_1 = require("./discussions_list/discussions_list.component");
var MyDatePipe_1 = require("../../MyDatePipe");
var episode_actions_component_1 = require("./episode_actions/episode_actions.component.");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var EpisodeComponent = (function () {
    function EpisodeComponent(episodesService, routeParams, service, responseHandler, title) {
        this.episodesService = episodesService;
        this.routeParams = routeParams;
        this.service = service;
        this.responseHandler = responseHandler;
        this.title = title;
    }
    EpisodeComponent.prototype.ngOnInit = function () {
        this.getEpisode();
        this.isLoggedIn = this.service.isLoggedIn();
    };
    EpisodeComponent.prototype.getEpisode = function () {
        var _this = this;
        this.episodesService.getEpisode(this.routeParams.get('id'))
            .subscribe(function (data) {
            _this.episode = data;
            _this.episode.poster = _this.service.getURL() + "shows/" + _this.episode.show_id + "/poster";
            _this.title.setTitle(_this.episode.show + " | " + _this.episode.name);
        }, function (error) { return _this.handleError(error); });
    };
    EpisodeComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    EpisodeComponent = __decorate([
        core_1.Component({
            selector: 'episode',
            templateUrl: 'app/components/episode/episode.html',
            providers: [episodes_service_1.EpisodesService],
            pipes: [MyDatePipe_1.MyDatePipe],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMRating_1.SMRating, watchers_component_1.WatchersComponent, discussions_list_component_1.DiscussionsListComponent, episode_actions_component_1.EpisodeActionsComponent]
        }), 
        __metadata('design:paramtypes', [episodes_service_1.EpisodesService, router_deprecated_1.RouteParams, global_service_1.GlobalService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], EpisodeComponent);
    return EpisodeComponent;
}());
exports.EpisodeComponent = EpisodeComponent;
//# sourceMappingURL=episode.component.js.map