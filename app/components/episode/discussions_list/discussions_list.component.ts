import {Component, Input, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {EpisodesService} from "../../../services/episodes.service";
import {MyDatePipe} from "../../../MyDatePipe";
import {DiscussionsService} from "../../../services/discussions.service";
import {GlobalService} from "../../../services/global.service";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'episode-discussions',
    templateUrl: 'app/components/episode/discussions_list/discussions_list.html',
    pipes: [MyDatePipe],
    directives: [ROUTER_DIRECTIVES],
    providers: [DiscussionsService]
})
export class DiscussionsListComponent implements OnInit {
    constructor(
        private episodesService:EpisodesService,
        private service:GlobalService,
        private responseHandler:ResponseHandler) {
    }

    @Input() episode_id;
    discussions;


    ngOnInit() {
        this.episodesService.getDiscussions(this.episode_id)
            .subscribe(
                data => {
                    this.discussions = data
                    this.discussions.forEach(discussion => discussion.user = this.service.getURL() + 'users/' + discussion.user_id + '/picture')
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)

    }
}