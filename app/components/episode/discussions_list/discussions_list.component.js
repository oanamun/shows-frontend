"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var episodes_service_1 = require("../../../services/episodes.service");
var MyDatePipe_1 = require("../../../MyDatePipe");
var discussions_service_1 = require("../../../services/discussions.service");
var global_service_1 = require("../../../services/global.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var DiscussionsListComponent = (function () {
    function DiscussionsListComponent(episodesService, service, responseHandler) {
        this.episodesService = episodesService;
        this.service = service;
        this.responseHandler = responseHandler;
    }
    DiscussionsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.episodesService.getDiscussions(this.episode_id)
            .subscribe(function (data) {
            _this.discussions = data;
            _this.discussions.forEach(function (discussion) { return discussion.user = _this.service.getURL() + 'users/' + discussion.user_id + '/picture'; });
        }, function (error) { return _this.handleError(error); });
    };
    DiscussionsListComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DiscussionsListComponent.prototype, "episode_id", void 0);
    DiscussionsListComponent = __decorate([
        core_1.Component({
            selector: 'episode-discussions',
            templateUrl: 'app/components/episode/discussions_list/discussions_list.html',
            pipes: [MyDatePipe_1.MyDatePipe],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [discussions_service_1.DiscussionsService]
        }), 
        __metadata('design:paramtypes', [episodes_service_1.EpisodesService, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], DiscussionsListComponent);
    return DiscussionsListComponent;
}());
exports.DiscussionsListComponent = DiscussionsListComponent;
//# sourceMappingURL=discussions_list.component.js.map