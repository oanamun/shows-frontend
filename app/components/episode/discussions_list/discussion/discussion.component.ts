import {Component, Input, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterLink, RouteParams, Router} from '@angular/router-deprecated';
import {DiscussionsService} from "../../../../services/discussions.service";
import {GlobalService} from "../../../../services/global.service";
import {MyDatePipe} from "../../../../MyDatePipe";
import {TimeAgoPipe} from "../../../../TimeAgoPipe";
import {ResponseHandler} from "../../../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'episode-discussions',
    templateUrl: 'app/components/episode/discussions_list/discussion/discussion.html',
    directives: [ROUTER_DIRECTIVES],
    pipes: [MyDatePipe, TimeAgoPipe],
    providers: [DiscussionsService]
})
export class DiscussionComponent implements OnInit {
    constructor(private discussionsService:DiscussionsService,
                private responseHandler:ResponseHandler,
                private routeParams:RouteParams,
                private router:Router,
                private service:GlobalService,
                private title:Title) {
    }

    discussion;
    comments;
    reply;

    ngOnInit() {
        let id = this.routeParams.get('id');
        this.getDiscussion(id);
        this.getComments(id);
    }

    getDiscussion(id) {
        this.discussionsService.getDiscussion(id)
            .subscribe(
                data => {
                    this.discussion = data
                    this.title.setTitle(this.discussion.episode+" | "+this.discussion.title)
                },
                error => this.handleError(error)
            );
    }

    getComments(id) {
        this.discussionsService.getComments(id)
            .subscribe(
                data => {
                    this.comments = data
                    this.comments.forEach(comment => this.getUser(comment))
                },
                error => this.handleError(error)
            );
    }


    replyToComment(discussion_id) {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login'])
            return;
        }

        this.discussionsService.addComment(this.reply, discussion_id)
            .subscribe(
                data => {
                    this.getUser(data);
                    this.comments.push(data);
                    this.reply = ""
                },
                error => this.handleError(error)
            );
    }

    getUser(comment) {
        comment.user = this.service.getURL() + 'users/' + comment.user_id + '/picture'
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}