"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var discussions_service_1 = require("../../../../services/discussions.service");
var global_service_1 = require("../../../../services/global.service");
var MyDatePipe_1 = require("../../../../MyDatePipe");
var TimeAgoPipe_1 = require("../../../../TimeAgoPipe");
var response_handler_service_1 = require("../../../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var DiscussionComponent = (function () {
    function DiscussionComponent(discussionsService, responseHandler, routeParams, router, service, title) {
        this.discussionsService = discussionsService;
        this.responseHandler = responseHandler;
        this.routeParams = routeParams;
        this.router = router;
        this.service = service;
        this.title = title;
    }
    DiscussionComponent.prototype.ngOnInit = function () {
        var id = this.routeParams.get('id');
        this.getDiscussion(id);
        this.getComments(id);
    };
    DiscussionComponent.prototype.getDiscussion = function (id) {
        var _this = this;
        this.discussionsService.getDiscussion(id)
            .subscribe(function (data) {
            _this.discussion = data;
            _this.title.setTitle(_this.discussion.episode + " | " + _this.discussion.title);
        }, function (error) { return _this.handleError(error); });
    };
    DiscussionComponent.prototype.getComments = function (id) {
        var _this = this;
        this.discussionsService.getComments(id)
            .subscribe(function (data) {
            _this.comments = data;
            _this.comments.forEach(function (comment) { return _this.getUser(comment); });
        }, function (error) { return _this.handleError(error); });
    };
    DiscussionComponent.prototype.replyToComment = function (discussion_id) {
        var _this = this;
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        this.discussionsService.addComment(this.reply, discussion_id)
            .subscribe(function (data) {
            _this.getUser(data);
            _this.comments.push(data);
            _this.reply = "";
        }, function (error) { return _this.handleError(error); });
    };
    DiscussionComponent.prototype.getUser = function (comment) {
        comment.user = this.service.getURL() + 'users/' + comment.user_id + '/picture';
    };
    DiscussionComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    DiscussionComponent = __decorate([
        core_1.Component({
            selector: 'episode-discussions',
            templateUrl: 'app/components/episode/discussions_list/discussion/discussion.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            pipes: [MyDatePipe_1.MyDatePipe, TimeAgoPipe_1.TimeAgoPipe],
            providers: [discussions_service_1.DiscussionsService]
        }), 
        __metadata('design:paramtypes', [discussions_service_1.DiscussionsService, response_handler_service_1.ResponseHandler, router_deprecated_1.RouteParams, router_deprecated_1.Router, global_service_1.GlobalService, platform_browser_1.Title])
    ], DiscussionComponent);
    return DiscussionComponent;
}());
exports.DiscussionComponent = DiscussionComponent;
//# sourceMappingURL=discussion.component.js.map