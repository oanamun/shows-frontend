import {Component, OnInit, Input, Output, EventEmitter}  from '@angular/core';
import {RouteParams, RouterLink, ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {EpisodesService} from "../../../services/episodes.service";
import {MyDatePipe} from "../../../MyDatePipe";
import {SMRating} from "../../../semantic/SMRating";
import {GlobalService} from "../../../services/global.service";
import {SMUserRating} from "../../../semantic/SMUserRating";
import {DiscussionsService} from "../../../services/discussions.service";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'episode-actions',
    templateUrl: 'app/components/episode/episode_actions/episode_actions.html',
    providers: [EpisodesService, DiscussionsService],
    pipes: [MyDatePipe],
    directives: [ROUTER_DIRECTIVES, SMRating, SMUserRating]
})

export class EpisodeActionsComponent implements OnInit {
    constructor(private episodesService:EpisodesService,
                private discussionsService:DiscussionsService,
                private routeParams:RouteParams,
                private service:GlobalService,
                private responseHandler:ResponseHandler) {
    }

    @Input() episode_id;
    @Output() rateEvent = new EventEmitter();
    isLoggedIn = false;
    watching = false;
    rating;
    discussion = {title: "", comments: ""};
    hasRating = false;

    ngOnInit() {
        let id = this.routeParams.get('id');
        this.isWatching()
        this.isLoggedIn = this.service.isLoggedIn()
    }

    getRating() {
        this.episodesService.getEpisodeRating(this.episode_id)
            .subscribe(
                data => {
                    this.rating = data.rating || 0
                    this.hasRating = true
                },
                error => this.hasRating = false
            );
    }

    watchEpisode() {
        this.episodesService.watchEpisode(this.episode_id)
            .subscribe(
                response => {
                    this.watching = !this.watching
                    this.hasRating = true;
                },
                error => this.handleError(error)
            );
    }

    openModal() {
        (<any>$('.ui.modal.discussion')).modal('show');
    }

    isWatching() {
        this.episodesService.isWatching(this.episode_id)
            .subscribe(
                data => {
                    this.watching = data.watching
                    if(this.watching){
                        this.getRating();
                    }
                },
                error => this.handleError(error)
            );
    }

    setEpisode() {
        localStorage.setItem("episode", this.episode_id)
        this.rateEvent.emit("new rating");
    }

    addDiscussion() {
        if (this.discussion.title.length == 0 || this.discussion.comments.length == 0) {
            this.responseHandler.errorMessage("The required fields are empty", 0)
            return;
        }

        this.episodesService.addDiscussion(this.discussion.title, this.episode_id)
            .subscribe(
                data => {
                    this.addComment(data.id)
                },
                error => this.handleError(error)
            );
    }

    addComment(discussion_id) {
        this.discussionsService.addComment(this.discussion.comments, discussion_id)
            .subscribe(
                data => {
                    this.responseHandler.successMessage("The discussion was poster")
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}