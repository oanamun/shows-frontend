"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var episodes_service_1 = require("../../../services/episodes.service");
var MyDatePipe_1 = require("../../../MyDatePipe");
var SMRating_1 = require("../../../semantic/SMRating");
var global_service_1 = require("../../../services/global.service");
var SMUserRating_1 = require("../../../semantic/SMUserRating");
var discussions_service_1 = require("../../../services/discussions.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var EpisodeActionsComponent = (function () {
    function EpisodeActionsComponent(episodesService, discussionsService, routeParams, service, responseHandler) {
        this.episodesService = episodesService;
        this.discussionsService = discussionsService;
        this.routeParams = routeParams;
        this.service = service;
        this.responseHandler = responseHandler;
        this.rateEvent = new core_1.EventEmitter();
        this.isLoggedIn = false;
        this.watching = false;
        this.discussion = { title: "", comments: "" };
        this.hasRating = false;
    }
    EpisodeActionsComponent.prototype.ngOnInit = function () {
        var id = this.routeParams.get('id');
        this.isWatching();
        this.isLoggedIn = this.service.isLoggedIn();
    };
    EpisodeActionsComponent.prototype.getRating = function () {
        var _this = this;
        this.episodesService.getEpisodeRating(this.episode_id)
            .subscribe(function (data) {
            _this.rating = data.rating || 0;
            _this.hasRating = true;
        }, function (error) { return _this.hasRating = false; });
    };
    EpisodeActionsComponent.prototype.watchEpisode = function () {
        var _this = this;
        this.episodesService.watchEpisode(this.episode_id)
            .subscribe(function (response) {
            _this.watching = !_this.watching;
            _this.hasRating = true;
        }, function (error) { return _this.handleError(error); });
    };
    EpisodeActionsComponent.prototype.openModal = function () {
        $('.ui.modal.discussion').modal('show');
    };
    EpisodeActionsComponent.prototype.isWatching = function () {
        var _this = this;
        this.episodesService.isWatching(this.episode_id)
            .subscribe(function (data) {
            _this.watching = data.watching;
            if (_this.watching) {
                _this.getRating();
            }
        }, function (error) { return _this.handleError(error); });
    };
    EpisodeActionsComponent.prototype.setEpisode = function () {
        localStorage.setItem("episode", this.episode_id);
        this.rateEvent.emit("new rating");
    };
    EpisodeActionsComponent.prototype.addDiscussion = function () {
        var _this = this;
        if (this.discussion.title.length == 0 || this.discussion.comments.length == 0) {
            this.responseHandler.errorMessage("The required fields are empty", 0);
            return;
        }
        this.episodesService.addDiscussion(this.discussion.title, this.episode_id)
            .subscribe(function (data) {
            _this.addComment(data.id);
        }, function (error) { return _this.handleError(error); });
    };
    EpisodeActionsComponent.prototype.addComment = function (discussion_id) {
        var _this = this;
        this.discussionsService.addComment(this.discussion.comments, discussion_id)
            .subscribe(function (data) {
            _this.responseHandler.successMessage("The discussion was poster");
        }, function (error) { return _this.handleError(error); });
    };
    EpisodeActionsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], EpisodeActionsComponent.prototype, "episode_id", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EpisodeActionsComponent.prototype, "rateEvent", void 0);
    EpisodeActionsComponent = __decorate([
        core_1.Component({
            selector: 'episode-actions',
            templateUrl: 'app/components/episode/episode_actions/episode_actions.html',
            providers: [episodes_service_1.EpisodesService, discussions_service_1.DiscussionsService],
            pipes: [MyDatePipe_1.MyDatePipe],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMRating_1.SMRating, SMUserRating_1.SMUserRating]
        }), 
        __metadata('design:paramtypes', [episodes_service_1.EpisodesService, discussions_service_1.DiscussionsService, router_deprecated_1.RouteParams, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], EpisodeActionsComponent);
    return EpisodeActionsComponent;
}());
exports.EpisodeActionsComponent = EpisodeActionsComponent;
//# sourceMappingURL=episode_actions.component..js.map