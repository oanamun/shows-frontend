import {Component} from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {LoginService} from "../../services/login.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'login',
    templateUrl: './app/components/login/login.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [LoginService]
})

export class LoginComponent {
    constructor(public loginService: LoginService,private title:Title) {
        title.setTitle("Login")
    }
    login(username, password) {
        this.loginService.authenticate(username, password)
    }
}
