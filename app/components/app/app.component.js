"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var nav_component_1 = require("../nav/nav.component");
var login_component_1 = require("../login/login.component");
var profile_component_1 = require("../profile/profile.component");
var home_component_1 = require("../home/home.component");
var shows_list_component_1 = require("../shows_list/shows_list.component");
var show_component_1 = require("../show/show.component");
var signup_component_1 = require("../signup/signup.component");
var activity_component_1 = require("../activity/activity.component");
var watchlist_component_1 = require("../watchlist/watchlist.component");
var episode_component_1 = require("../episode/episode.component");
var settings_component_1 = require("../profile/settings/settings.component");
var notifications_component_1 = require("../notifications/notifications.component");
var discussion_component_1 = require("../episode/discussions_list/discussion/discussion.component");
var review_component_1 = require("../show/reviews_list/review/review.component");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: './app/components/app/app.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, nav_component_1.NavComponent]
        }),
        router_deprecated_1.RouteConfig([
            { path: '/', name: 'Home', component: home_component_1.HomeComponent, useAsDefault: true },
            { path: '/login', name: 'Login', component: login_component_1.LoginComponent },
            { path: '/signup', name: 'Signup', component: signup_component_1.SignupComponent },
            { path: '/shows', name: 'ShowsList', component: shows_list_component_1.ShowsListComponent },
            { path: '/watchlist', name: 'Watchlist', component: watchlist_component_1.WatchlistComponent },
            { path: '/show/:id', name: 'Show', component: show_component_1.ShowComponent },
            { path: '/episode/:id', name: 'Episode', component: episode_component_1.EpisodeComponent },
            { path: '/review/:id', name: 'Review', component: review_component_1.ReviewComponent },
            { path: '/discussion/:id', name: 'Discussion', component: discussion_component_1.DiscussionComponent },
            { path: '/profile/:id', name: 'Profile', component: profile_component_1.ProfileComponent },
            { path: '/activity', name: 'Activity', component: activity_component_1.ActivityComponent },
            { path: '/settings', name: 'Settings', component: settings_component_1.SettingsComponent },
            { path: '/notifications', name: 'Notifications', component: notifications_component_1.NotificationsComponent }
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map