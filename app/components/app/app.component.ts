import {Component}              from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {NavComponent}           from "../nav/nav.component";
import {LoginComponent}         from "../login/login.component";
import {ProfileComponent}       from "../profile/profile.component";
import {GlobalService}          from "../../services/global.service";
import {HomeComponent}          from "../home/home.component";
import {ShowsListComponent}     from "../shows_list/shows_list.component";
import {ShowComponent}          from "../show/show.component";
import {SignupComponent}        from "../signup/signup.component";
import {ActivityComponent}      from "../activity/activity.component";
import {WatchlistComponent}     from "../watchlist/watchlist.component";
import {EpisodeComponent}       from "../episode/episode.component";
import {SettingsComponent}      from "../profile/settings/settings.component";
import {NotificationsComponent} from "../notifications/notifications.component";
import {DiscussionComponent} from "../episode/discussions_list/discussion/discussion.component";
import {ReviewComponent} from "../show/reviews_list/review/review.component";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'app',
    templateUrl: './app/components/app/app.html',
    directives: [ROUTER_DIRECTIVES, NavComponent]
})

@RouteConfig([
    {path: '/', name: 'Home', component: HomeComponent, useAsDefault: true},
    {path: '/login', name: 'Login', component: LoginComponent},
    {path: '/signup', name: 'Signup', component: SignupComponent},
    {path: '/shows', name: 'ShowsList', component: ShowsListComponent},
    {path: '/watchlist', name: 'Watchlist', component: WatchlistComponent},
    {path: '/show/:id', name: 'Show', component: ShowComponent},
    {path: '/episode/:id', name: 'Episode', component: EpisodeComponent},
    {path: '/review/:id', name: 'Review', component: ReviewComponent},
    {path: '/discussion/:id', name: 'Discussion', component: DiscussionComponent},
    {path: '/profile/:id', name: 'Profile', component: ProfileComponent},
    {path: '/activity', name: 'Activity', component: ActivityComponent},
    {path: '/settings', name: 'Settings', component: SettingsComponent},
    {path: '/notifications', name: 'Notifications', component: NotificationsComponent}
])

export class AppComponent {

}