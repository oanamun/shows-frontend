import {Component, OnInit}  from '@angular/core';
import {RouteParams, RouterLink, ROUTER_DIRECTIVES, Router}  from '@angular/router-deprecated';
import {ShowsService} from "../../services/shows.service";
import {Show} from "../../model/show";
import {GlobalService} from "../../services/global.service";
import {SMRating} from "../../semantic/SMRating";
import {ShowActionsComponent} from "./show_actions/show_actions.component";
import {WatchersComponent} from "./watchers/watchers.component";
import {SeasonsComponent} from "./seasons/seasons.component";
import {ReviewsListComponent} from "./reviews_list/reviews_list.component";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'show',
    templateUrl: 'app/components/show/show.html',
    providers: [ShowsService],
    directives: [ROUTER_DIRECTIVES, SMRating, ReviewsListComponent, ShowActionsComponent, WatchersComponent, SeasonsComponent]
})

export class ShowComponent implements OnInit {
    constructor(
        private _showsService:ShowsService,
        private _routeParams:RouteParams,
        private service:GlobalService,
        private responseHandler:ResponseHandler,
        private title:Title) {}

    show;
    posterUrl:string;
    headerUrl:string;
    categories;
    isLoggedIn:boolean = false;

    ngOnInit() {
        let id = this._routeParams.get('id');
        this.getShow(id);
        this.getCategories(id);

        this.isLoggedIn = this.service.isLoggedIn();
    }

    getShow(id) {
        this._showsService.getShow(id)
            .subscribe(
                data => {
                    this.show = data
                    this.posterUrl = this.service.getURL() + 'shows/' + this.show.id + '/poster'
                    this.title.setTitle(this.show.name)
                },
                error => this.handleError(error)
            );
    }

    getCategories(id) {
        this._showsService.getShowCategories(id)
            .subscribe(
                data => this.categories = data,
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}