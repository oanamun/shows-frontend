"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shows_service_1 = require("../../../services/shows.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var SMUserRating_1 = require("../../../semantic/SMUserRating");
var SMProgress_1 = require("../../../semantic/SMProgress");
var profile_service_1 = require("../../../services/profile.service");
var global_service_1 = require("../../../services/global.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var ShowActionsComponent = (function () {
    function ShowActionsComponent(showsService, router, profileService, service, responseHandler) {
        this.showsService = showsService;
        this.router = router;
        this.profileService = profileService;
        this.service = service;
        this.responseHandler = responseHandler;
        this.hasRating = false;
        this.hasPercent = false;
        this.review = { title: '', message: '' };
    }
    ShowActionsComponent.prototype.ngOnInit = function () {
        this.isWatching();
        this.getPercent();
        this.getFriends();
    };
    ShowActionsComponent.prototype.isWatching = function () {
        var _this = this;
        this.showsService.isWatching(this.show_id)
            .subscribe(function (data) {
            _this.watching = data.watching;
            if (_this.watching) {
                _this.getRating();
            }
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.getRating = function () {
        var _this = this;
        this.showsService.getShowRating(this.show_id)
            .subscribe(function (data) {
            _this.rating = data.rating || 0;
            _this.hasRating = true;
        }, function (error) { return _this.hasRating = false; });
    };
    ShowActionsComponent.prototype.getPercent = function () {
        var _this = this;
        this.showsService.getWatchPercent(this.show_id)
            .subscribe(function (data) {
            _this.percent = data.percent || 0;
            _this.hasPercent = true;
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.getFriends = function () {
        var _this = this;
        this.profileService.getFollowing(this.service.getUser().id)
            .subscribe(function (data) {
            _this.friends = data;
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.watchShow = function () {
        var _this = this;
        this.showsService.watchShow(this.show_id)
            .subscribe(function (response) {
            _this.watching = !_this.watching;
            _this.rating = 0;
            _this.hasRating = true;
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.rateShow = function (rating) {
        var _this = this;
        this.showsService.rateShow(this.show_id, rating)
            .subscribe(function (response) {
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.showSelect = function () {
        document.getElementById("rec-select").style.display = "block";
    };
    ShowActionsComponent.prototype.onSelectChange = function (value) {
        this.friend_id = value;
    };
    ShowActionsComponent.prototype.recommend = function () {
        var _this = this;
        this.showsService.recommendShow(this.show_id, this.friend_id)
            .subscribe(function (response) {
            document.getElementById("rec-select").style.display = "none";
            _this.responseHandler.successMessage("The recommendation was sent");
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.openModal = function () {
        $('.ui.modal.review').modal('show');
    };
    ShowActionsComponent.prototype.submitReview = function () {
        var _this = this;
        if (this.review.title.length == 0 || this.review.message.length == 0) {
            this.responseHandler.errorMessage("The required fields are empty", 0);
            return;
        }
        this.showsService.reviewShow(this.show_id, this.review)
            .subscribe(function (response) {
            _this.responseHandler.successMessage("The review was submitted");
        }, function (error) { return _this.handleError(error); });
    };
    ShowActionsComponent.prototype.setShow = function () {
        localStorage.setItem("show", this.show_id);
    };
    ShowActionsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ShowActionsComponent.prototype, "show_id", void 0);
    ShowActionsComponent = __decorate([
        core_1.Component({
            selector: 'show-actions',
            templateUrl: 'app/components/show/show_actions/show_actions.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMUserRating_1.SMUserRating, SMProgress_1.SMProgress],
            providers: [shows_service_1.ShowsService, profile_service_1.ProfileService]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, router_deprecated_1.Router, profile_service_1.ProfileService, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], ShowActionsComponent);
    return ShowActionsComponent;
}());
exports.ShowActionsComponent = ShowActionsComponent;
//# sourceMappingURL=show_actions.component.js.map