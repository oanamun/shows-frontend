import {Component, Input, OnInit} from '@angular/core';
import {ShowsService} from "../../../services/shows.service";
import {ROUTER_DIRECTIVES, RouterLink, Router} from '@angular/router-deprecated';
import {SMUserRating} from "../../../semantic/SMUserRating";
import {SMProgress} from "../../../semantic/SMProgress";
import {ProfileService} from "../../../services/profile.service";
import {GlobalService} from "../../../services/global.service";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'show-actions',
    templateUrl: 'app/components/show/show_actions/show_actions.html',
    directives: [ROUTER_DIRECTIVES, SMUserRating, SMProgress],
    providers: [ShowsService, ProfileService]
})

export class ShowActionsComponent implements OnInit {
    constructor(private showsService:ShowsService,
                private router:Router,
                private profileService:ProfileService,
                private service:GlobalService,
                private responseHandler:ResponseHandler) {
    }

    @Input() show_id;
    watching;
    rating;
    percent;
    hasRating = false;
    hasPercent = false;
    friends;
    friend_id;
    review = {title: '', message: ''};

    ngOnInit() {
        this.isWatching()
        this.getPercent()
        this.getFriends()
    }

    isWatching() {
        this.showsService.isWatching(this.show_id)
            .subscribe(
                data => {
                    this.watching = data.watching
                    if (this.watching) {
                        this.getRating()
                    }
                },
                error => this.handleError(error)
            );
    }

    getRating() {
        this.showsService.getShowRating(this.show_id)
            .subscribe(
                data => {
                    this.rating = data.rating || 0
                    this.hasRating = true
                },
                error => this.hasRating=false
            );
    }

    getPercent() {
        this.showsService.getWatchPercent(this.show_id)
            .subscribe(
                data => {
                    this.percent = data.percent || 0
                    this.hasPercent = true
                },
                error => this.handleError(error)
            );
    }

    getFriends() {
        this.profileService.getFollowing(this.service.getUser().id)
            .subscribe(
                data => {
                    this.friends = data
                },
                error => this.handleError(error)
            );
    }

    watchShow() {
        this.showsService.watchShow(this.show_id)
            .subscribe(
                response => {
                    this.watching = !this.watching
                    this.rating = 0
                    this.hasRating = true
                },
                error => this.handleError(error)
            );
    }

    rateShow(rating) {
        this.showsService.rateShow(this.show_id, rating)
            .subscribe(
                response => {
                },
                error => this.handleError(error)
            );
    }

    showSelect() {
        document.getElementById("rec-select").style.display = "block";
    }

    onSelectChange(value) {
        this.friend_id = value;
    }

    recommend() {
        this.showsService.recommendShow(this.show_id, this.friend_id)
            .subscribe(
                response => {
                    document.getElementById("rec-select").style.display = "none";
                    this.responseHandler.successMessage("The recommendation was sent")
                },
                error => this.handleError(error)
            );
    }

    openModal() {
        (<any>$('.ui.modal.review')).modal('show');
    }

    submitReview() {
        if (this.review.title.length == 0 || this.review.message.length == 0) {
            this.responseHandler.errorMessage("The required fields are empty", 0)
            return;
        }

        this.showsService.reviewShow(this.show_id, this.review)
            .subscribe(
                response => {
                    this.responseHandler.successMessage("The review was submitted")
                },
                error => this.handleError(error)
            );
    }

    setShow() {
        localStorage.setItem("show", this.show_id)
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}