import {Component, Input, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {EpisodesService} from "../../../services/episodes.service";
import {ShowsService} from "../../../services/shows.service";
import {GlobalService} from "../../../services/global.service";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'watchers-list',
    template: `
    <div *ngIf="watchers" class="watchers ui card">
        <div class="content">
            <a class="ui ribbon label">Watchers</a>
            <div class="ui divided list">
                <div *ngFor="let user of watchers" class="item">
                    <img [routerLink]="['Profile',{ id: user.id }]" class="ui avatar image" src="{{user.picture}}">
                <span class="content">
                    <a [routerLink]="['Profile',{ id: user.id }]" class="header">
                        {{user.username}}
                    </a>
                </span>
                </div>
            </div>
        </div>
    </div>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [ShowsService, EpisodesService]
})
export class WatchersComponent implements OnInit {
    constructor(
        private showsService:ShowsService,
        private episodesServie:EpisodesService,
        private service:GlobalService,
        private responseHandler:ResponseHandler) {
    }

    @Input() show_id;
    @Input() episode_id;
    watchers;

    ngOnInit() {
       if(this.show_id){
           this.getShowWatchers()
       }
        else if(this.episode_id){
           this.getEpisodeWatchers()
       }
    }

    getShowWatchers(){
        this.showsService.getShowWatchers(this.show_id)
            .subscribe(
                data => {
                    if(data.length==0){
                        return
                    }
                    this.watchers = data
                    this.watchers.forEach(user => user.picture = this.service.getURL() + 'users/' + user.id + '/picture')
                },
                error => this.handleError(error)
            );
    }

    getEpisodeWatchers(){
        this.episodesServie.getEpisodeWatchers(this.episode_id)
            .subscribe(
                data => {
                    if(data.length==0){
                        return
                    }
                    this.watchers = data
                    this.watchers.forEach(user => user.picture = this.service.getURL() + 'users/' + user.id + '/picture')
                },
                error => this.handleError(error)
            );
    }



    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}