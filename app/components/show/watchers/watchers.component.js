"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var episodes_service_1 = require("../../../services/episodes.service");
var shows_service_1 = require("../../../services/shows.service");
var global_service_1 = require("../../../services/global.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var WatchersComponent = (function () {
    function WatchersComponent(showsService, episodesServie, service, responseHandler) {
        this.showsService = showsService;
        this.episodesServie = episodesServie;
        this.service = service;
        this.responseHandler = responseHandler;
    }
    WatchersComponent.prototype.ngOnInit = function () {
        if (this.show_id) {
            this.getShowWatchers();
        }
        else if (this.episode_id) {
            this.getEpisodeWatchers();
        }
    };
    WatchersComponent.prototype.getShowWatchers = function () {
        var _this = this;
        this.showsService.getShowWatchers(this.show_id)
            .subscribe(function (data) {
            if (data.length == 0) {
                return;
            }
            _this.watchers = data;
            _this.watchers.forEach(function (user) { return user.picture = _this.service.getURL() + 'users/' + user.id + '/picture'; });
        }, function (error) { return _this.handleError(error); });
    };
    WatchersComponent.prototype.getEpisodeWatchers = function () {
        var _this = this;
        this.episodesServie.getEpisodeWatchers(this.episode_id)
            .subscribe(function (data) {
            if (data.length == 0) {
                return;
            }
            _this.watchers = data;
            _this.watchers.forEach(function (user) { return user.picture = _this.service.getURL() + 'users/' + user.id + '/picture'; });
        }, function (error) { return _this.handleError(error); });
    };
    WatchersComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WatchersComponent.prototype, "show_id", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], WatchersComponent.prototype, "episode_id", void 0);
    WatchersComponent = __decorate([
        core_1.Component({
            selector: 'watchers-list',
            template: "\n    <div *ngIf=\"watchers\" class=\"watchers ui card\">\n        <div class=\"content\">\n            <a class=\"ui ribbon label\">Watchers</a>\n            <div class=\"ui divided list\">\n                <div *ngFor=\"let user of watchers\" class=\"item\">\n                    <img [routerLink]=\"['Profile',{ id: user.id }]\" class=\"ui avatar image\" src=\"{{user.picture}}\">\n                <span class=\"content\">\n                    <a [routerLink]=\"['Profile',{ id: user.id }]\" class=\"header\">\n                        {{user.username}}\n                    </a>\n                </span>\n                </div>\n            </div>\n        </div>\n    </div>\n    ",
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [shows_service_1.ShowsService, episodes_service_1.EpisodesService]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, episodes_service_1.EpisodesService, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], WatchersComponent);
    return WatchersComponent;
}());
exports.WatchersComponent = WatchersComponent;
//# sourceMappingURL=watchers.component.js.map