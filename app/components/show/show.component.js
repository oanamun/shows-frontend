"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var shows_service_1 = require("../../services/shows.service");
var global_service_1 = require("../../services/global.service");
var SMRating_1 = require("../../semantic/SMRating");
var show_actions_component_1 = require("./show_actions/show_actions.component");
var watchers_component_1 = require("./watchers/watchers.component");
var seasons_component_1 = require("./seasons/seasons.component");
var reviews_list_component_1 = require("./reviews_list/reviews_list.component");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var ShowComponent = (function () {
    function ShowComponent(_showsService, _routeParams, service, responseHandler, title) {
        this._showsService = _showsService;
        this._routeParams = _routeParams;
        this.service = service;
        this.responseHandler = responseHandler;
        this.title = title;
        this.isLoggedIn = false;
    }
    ShowComponent.prototype.ngOnInit = function () {
        var id = this._routeParams.get('id');
        this.getShow(id);
        this.getCategories(id);
        this.isLoggedIn = this.service.isLoggedIn();
    };
    ShowComponent.prototype.getShow = function (id) {
        var _this = this;
        this._showsService.getShow(id)
            .subscribe(function (data) {
            _this.show = data;
            _this.posterUrl = _this.service.getURL() + 'shows/' + _this.show.id + '/poster';
            _this.title.setTitle(_this.show.name);
        }, function (error) { return _this.handleError(error); });
    };
    ShowComponent.prototype.getCategories = function (id) {
        var _this = this;
        this._showsService.getShowCategories(id)
            .subscribe(function (data) { return _this.categories = data; }, function (error) { return _this.handleError(error); });
    };
    ShowComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    ShowComponent = __decorate([
        core_1.Component({
            selector: 'show',
            templateUrl: 'app/components/show/show.html',
            providers: [shows_service_1.ShowsService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMRating_1.SMRating, reviews_list_component_1.ReviewsListComponent, show_actions_component_1.ShowActionsComponent, watchers_component_1.WatchersComponent, seasons_component_1.SeasonsComponent]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, router_deprecated_1.RouteParams, global_service_1.GlobalService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], ShowComponent);
    return ShowComponent;
}());
exports.ShowComponent = ShowComponent;
//# sourceMappingURL=show.component.js.map