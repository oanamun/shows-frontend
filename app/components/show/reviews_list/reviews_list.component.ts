import {Component, Input,OnInit} from '@angular/core';
import {ShowsService} from "../../../services/shows.service";
import {ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {SMRating} from "../../../semantic/SMRating";
import {GlobalService} from "../../../services/global.service";
import {MyDatePipe} from "../../../MyDatePipe";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'show-reviews',
    templateUrl: 'app/components/show/reviews_list/reviews_list.html',
    directives: [ROUTER_DIRECTIVES, SMRating],
    pipes: [MyDatePipe],
    providers: [ShowsService]
})
export class ReviewsListComponent implements OnInit {
    constructor(private showsService:ShowsService, private service:GlobalService, private responseHandler:ResponseHandler) {
    }

    @Input() show_id;
    reviews;

    ngOnInit() {
        this.showsService.getReviews(this.show_id)
            .subscribe(
                data => {
                    if(data.length==0){
                        return
                    }
                    this.reviews = data
                    this.reviews.forEach(review => this.getUser(review))
                },
                error => this.handleError(error)
            );
    }

    getUser(review) {
        review.user = this.service.getURL() + 'users/' + review.user_id + '/picture'
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}