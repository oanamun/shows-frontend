"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shows_service_1 = require("../../../services/shows.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var SMRating_1 = require("../../../semantic/SMRating");
var global_service_1 = require("../../../services/global.service");
var MyDatePipe_1 = require("../../../MyDatePipe");
var response_handler_service_1 = require("../../../services/response_handler.service");
var ReviewsListComponent = (function () {
    function ReviewsListComponent(showsService, service, responseHandler) {
        this.showsService = showsService;
        this.service = service;
        this.responseHandler = responseHandler;
    }
    ReviewsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showsService.getReviews(this.show_id)
            .subscribe(function (data) {
            if (data.length == 0) {
                return;
            }
            _this.reviews = data;
            _this.reviews.forEach(function (review) { return _this.getUser(review); });
        }, function (error) { return _this.handleError(error); });
    };
    ReviewsListComponent.prototype.getUser = function (review) {
        review.user = this.service.getURL() + 'users/' + review.user_id + '/picture';
    };
    ReviewsListComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ReviewsListComponent.prototype, "show_id", void 0);
    ReviewsListComponent = __decorate([
        core_1.Component({
            selector: 'show-reviews',
            templateUrl: 'app/components/show/reviews_list/reviews_list.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMRating_1.SMRating],
            pipes: [MyDatePipe_1.MyDatePipe],
            providers: [shows_service_1.ShowsService]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], ReviewsListComponent);
    return ReviewsListComponent;
}());
exports.ReviewsListComponent = ReviewsListComponent;
//# sourceMappingURL=reviews_list.component.js.map