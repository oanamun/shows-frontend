import {Component, Input,OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterLink, RouteParams, Router} from '@angular/router-deprecated';
import {ReviewsService} from "../../../../services/reviews.service";
import {GlobalService} from "../../../../services/global.service";
import {MyDatePipe} from "../../../../MyDatePipe";
import {TimeAgoPipe} from "../../../../TimeAgoPipe";
import {ResponseHandler} from "../../../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'review',
    templateUrl: 'app/components/show/reviews_list/review/review.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ReviewsService],
    pipes: [MyDatePipe, TimeAgoPipe]
})
export class ReviewComponent implements OnInit {
    constructor(
        private reviewsService:ReviewsService,
        private router:Router,
        private routeParams:RouteParams,
        private service:GlobalService,
        private responseHandler:ResponseHandler,
        private title:Title) {
    }

    review;
    comments;
    reply;

    ngOnInit() {
        let id = this.routeParams.get('id');
        this.getReview(id);
        this.getComments(id);

        (<any>$('#review-comment')).trumbowyg();
    }

    getReview(id) {
        this.reviewsService.getReview(id)
            .subscribe(
                data => {
                    this.review = data
                    this.review.user = this.service.getURL() + 'users/' + this.review.user_id + '/picture'
                    this.review.poster = this.service.getURL()+"shows/"+this.review.show_id+"/poster"
                    this.title.setTitle(this.review.show+" | "+this.review.title)
                },
                error => this.handleError(error)
            );
    }

    getComments(id) {
        this.reviewsService.getComments(id)
            .subscribe(
                data => {
                    this.comments = data
                    this.comments.forEach(comment => this.getUser(comment))
                },
                error => this.handleError(error)
            );
    }

    replyToComment(id) {
        if(!this.service.isLoggedIn()){
            this.router.navigate(['Login'])
            return;
        }

        this.reviewsService.addComment(this.reply, id)
            .subscribe(
                data => {
                    this.getUser(data);
                    data.username = this.service.getUser().username
                    this.comments.push(data);
                },
                error => this.handleError(error)
            );
        this.reply=""
    }

    getUser(comment) {
        comment.user = this.service.getURL() + 'users/' + comment.user_id + '/picture'
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}