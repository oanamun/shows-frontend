"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var reviews_service_1 = require("../../../../services/reviews.service");
var global_service_1 = require("../../../../services/global.service");
var MyDatePipe_1 = require("../../../../MyDatePipe");
var TimeAgoPipe_1 = require("../../../../TimeAgoPipe");
var response_handler_service_1 = require("../../../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var ReviewComponent = (function () {
    function ReviewComponent(reviewsService, router, routeParams, service, responseHandler, title) {
        this.reviewsService = reviewsService;
        this.router = router;
        this.routeParams = routeParams;
        this.service = service;
        this.responseHandler = responseHandler;
        this.title = title;
    }
    ReviewComponent.prototype.ngOnInit = function () {
        var id = this.routeParams.get('id');
        this.getReview(id);
        this.getComments(id);
        $('#review-comment').trumbowyg();
    };
    ReviewComponent.prototype.getReview = function (id) {
        var _this = this;
        this.reviewsService.getReview(id)
            .subscribe(function (data) {
            _this.review = data;
            _this.review.user = _this.service.getURL() + 'users/' + _this.review.user_id + '/picture';
            _this.review.poster = _this.service.getURL() + "shows/" + _this.review.show_id + "/poster";
            _this.title.setTitle(_this.review.show + " | " + _this.review.title);
        }, function (error) { return _this.handleError(error); });
    };
    ReviewComponent.prototype.getComments = function (id) {
        var _this = this;
        this.reviewsService.getComments(id)
            .subscribe(function (data) {
            _this.comments = data;
            _this.comments.forEach(function (comment) { return _this.getUser(comment); });
        }, function (error) { return _this.handleError(error); });
    };
    ReviewComponent.prototype.replyToComment = function (id) {
        var _this = this;
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        this.reviewsService.addComment(this.reply, id)
            .subscribe(function (data) {
            _this.getUser(data);
            data.username = _this.service.getUser().username;
            _this.comments.push(data);
        }, function (error) { return _this.handleError(error); });
        this.reply = "";
    };
    ReviewComponent.prototype.getUser = function (comment) {
        comment.user = this.service.getURL() + 'users/' + comment.user_id + '/picture';
    };
    ReviewComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    ReviewComponent = __decorate([
        core_1.Component({
            selector: 'review',
            templateUrl: 'app/components/show/reviews_list/review/review.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [reviews_service_1.ReviewsService],
            pipes: [MyDatePipe_1.MyDatePipe, TimeAgoPipe_1.TimeAgoPipe]
        }), 
        __metadata('design:paramtypes', [reviews_service_1.ReviewsService, router_deprecated_1.Router, router_deprecated_1.RouteParams, global_service_1.GlobalService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], ReviewComponent);
    return ReviewComponent;
}());
exports.ReviewComponent = ReviewComponent;
//# sourceMappingURL=review.component.js.map