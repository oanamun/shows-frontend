"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shows_service_1 = require("../../../services/shows.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var episodes_service_1 = require("../../../services/episodes.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var SeasonsComponent = (function () {
    function SeasonsComponent(showsService, episodesService, responseHandler) {
        this.showsService = showsService;
        this.episodesService = episodesService;
        this.responseHandler = responseHandler;
    }
    SeasonsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showsService.getSeasons(this.show_id)
            .subscribe(function (data) { return _this.seasons = data; }, function (error) { return _this.handleError(error); });
        jQuery('.my-dropdown').dropdown();
    };
    SeasonsComponent.prototype.onSelectChange = function (id) {
        var _this = this;
        this.episodesService.getEpisodes(id)
            .subscribe(function (data) { return _this.episodes = data; }, function (error) { return _this.handleError(error); });
    };
    SeasonsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], SeasonsComponent.prototype, "show_id", void 0);
    SeasonsComponent = __decorate([
        core_1.Component({
            selector: 'show-seasons',
            template: "\n        <div class=\"seasons ui card\">\n            <div class=\"content\">\n                <a class=\"header\">Seasons</a>\n                    <select #select (change)=\"onSelectChange(select.value)\" class=\"my-dropdown ui dropdown\">\n                        <option value=\"\">Select...</option>\n                        <option *ngFor=\"let season of seasons\" [value]=\"season.id\">\n                               Season {{season.number}}\n                        </option>\n                    </select>\n                    <div *ngIf=\"episodes\" class=\"ui middle aligned divided list\">\n                       <div class=\"ui sub header\">Episodes</div>\n                        <div *ngFor=\"let episode of episodes; let i = index\" class=\"item\">\n                            <a [routerLink]=\"['Episode',{ id: episode.id }]\" class=\"header\">\n                                <a class=\"ui label\">{{i+1}}</a>\n                                {{episode.name}}\n                            </a>\n                        </div>\n                    </div>\n            </div>\n        </div>\n    ",
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [episodes_service_1.EpisodesService]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, episodes_service_1.EpisodesService, response_handler_service_1.ResponseHandler])
    ], SeasonsComponent);
    return SeasonsComponent;
}());
exports.SeasonsComponent = SeasonsComponent;
//# sourceMappingURL=seasons.component.js.map