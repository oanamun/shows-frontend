import {Component, Input, OnInit} from '@angular/core';
import {ShowsService} from "../../../services/shows.service";
import {ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {GlobalService} from "../../../services/global.service";
import {EpisodesService} from "../../../services/episodes.service";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'show-seasons',
    template: `
        <div class="seasons ui card">
            <div class="content">
                <a class="header">Seasons</a>
                    <select #select (change)="onSelectChange(select.value)" class="my-dropdown ui dropdown">
                        <option value="">Select...</option>
                        <option *ngFor="let season of seasons" [value]="season.id">
                               Season {{season.number}}
                        </option>
                    </select>
                    <div *ngIf="episodes" class="ui middle aligned divided list">
                       <div class="ui sub header">Episodes</div>
                        <div *ngFor="let episode of episodes; let i = index" class="item">
                            <a [routerLink]="['Episode',{ id: episode.id }]" class="header">
                                <a class="ui label">{{i+1}}</a>
                                {{episode.name}}
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [EpisodesService]
})
export class SeasonsComponent implements OnInit {
    constructor(private showsService:ShowsService, private episodesService:EpisodesService,private responseHandler:ResponseHandler) {
    }

    @Input() show_id;
    seasons;
    episodes;

    ngOnInit() {
        this.showsService.getSeasons(this.show_id)
            .subscribe(
                data => this.seasons = data,
                error => this.handleError(error)
            );

        (<any>jQuery('.my-dropdown')).dropdown();

    }

    onSelectChange(id) {
        this.episodesService.getEpisodes(id)
            .subscribe(
                data => this.episodes = data,
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}