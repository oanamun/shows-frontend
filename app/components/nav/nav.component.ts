import {Component, OnInit} from '@angular/core';
import {GlobalService}  from '../../services/global.service';
import {User} from '../../model/user'
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {UserNavComponent} from "./nav_user/user_nav.component";

@Component({
    selector: 'nav',
    templateUrl: 'app/components/nav/nav.html',
    directives: [ROUTER_DIRECTIVES, UserNavComponent]

})
export class NavComponent implements OnInit {
    constructor(private service:GlobalService, private router:Router) {
        this.user = this.service.getUser();
    }

    user:User;

    get loggedIn(){
        return this.service.isLoggedIn();
    }

    ngOnInit() {
        (<any>$('#shows-search'))
            .search({
            apiSettings: {
                url: this.service.getURL()+'shows?name={query}'
            },
            fields: {
                results: 'filtered',
                title   : 'name',
                description: 'channel',
                url: 'url'
            },
            minCharacters : 1,
            cache: true
        });
    }
}