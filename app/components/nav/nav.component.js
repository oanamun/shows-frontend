"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var global_service_1 = require('../../services/global.service');
var router_deprecated_1 = require('@angular/router-deprecated');
var user_nav_component_1 = require("./nav_user/user_nav.component");
var NavComponent = (function () {
    function NavComponent(service, router) {
        this.service = service;
        this.router = router;
        this.user = this.service.getUser();
    }
    Object.defineProperty(NavComponent.prototype, "loggedIn", {
        get: function () {
            return this.service.isLoggedIn();
        },
        enumerable: true,
        configurable: true
    });
    NavComponent.prototype.ngOnInit = function () {
        $('#shows-search')
            .search({
            apiSettings: {
                url: this.service.getURL() + 'shows?name={query}'
            },
            fields: {
                results: 'filtered',
                title: 'name',
                description: 'channel',
                url: 'url'
            },
            minCharacters: 1,
            cache: true
        });
    };
    NavComponent = __decorate([
        core_1.Component({
            selector: 'nav',
            templateUrl: 'app/components/nav/nav.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, user_nav_component_1.UserNavComponent]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, router_deprecated_1.Router])
    ], NavComponent);
    return NavComponent;
}());
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map