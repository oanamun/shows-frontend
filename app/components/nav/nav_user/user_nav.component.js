"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("../../../services/global.service");
var profile_service_1 = require("../../../services/profile.service");
var UserNavComponent = (function () {
    function UserNavComponent(service, profileService, router) {
        this.service = service;
        this.profileService = profileService;
        this.router = router;
    }
    Object.defineProperty(UserNavComponent.prototype, "user", {
        get: function () {
            return this.service.getUser();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserNavComponent.prototype, "pictureUrl", {
        get: function () {
            return this.service.getURL() + 'users/' + this.service.getUser().id + '/picture';
        },
        enumerable: true,
        configurable: true
    });
    UserNavComponent.prototype.logout = function () {
        this.service.logout();
        this.router.navigate(['/Home']);
    };
    UserNavComponent.prototype.openNotifications = function () {
        $('.icon .nav')
            .popup({
            on: 'click'
        });
        this.getNotifications();
    };
    UserNavComponent.prototype.getNotifications = function () {
        var _this = this;
        this.profileService.getNotifications()
            .subscribe(function (data) {
            _this.notifications = data;
            _this.notifications.forEach(function (notification) { return _this.getUser(notification); });
            _this.notifications.sort(_this.compare);
            console.log(_this.notifications);
        }, function (error) { return _this.handleError(error); });
    };
    UserNavComponent.prototype.compare = function (a, b) {
        if (a.created_at < b.created_at)
            return 1;
        else if (a.created_at > b.created_at)
            return -1;
        else
            return 0;
    };
    UserNavComponent.prototype.getUser = function (notification) {
        if (notification.follower_id) {
            notification.picture = this.service.getURL() + 'users/' + notification.follower_id + '/picture';
        }
    };
    UserNavComponent.prototype.handleError = function (error) {
        console.log(error);
    };
    UserNavComponent = __decorate([
        core_1.Component({
            selector: 'user-nav',
            templateUrl: 'app/components/nav/nav_user/user_nav.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [profile_service_1.ProfileService]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, profile_service_1.ProfileService, router_deprecated_1.Router])
    ], UserNavComponent);
    return UserNavComponent;
}());
exports.UserNavComponent = UserNavComponent;
//# sourceMappingURL=user_nav.component.js.map