import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {GlobalService} from "../../../services/global.service";
import {User} from "../../../model/user";
import {ProfileService} from "../../../services/profile.service";

@Component({
    selector: 'user-nav',
    templateUrl: 'app/components/nav/nav_user/user_nav.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ProfileService]
})
export class UserNavComponent {
    constructor(private service:GlobalService, private profileService:ProfileService, private router:Router) {

    }
    notifications;

    get user(){
        return this.service.getUser();
    }

    get pictureUrl(){
        return this.service.getURL() + 'users/' + this.service.getUser().id + '/picture'
    }

    logout(){
        this.service.logout();
        this.router.navigate(['/Home'])
    }

    openNotifications(){
        (<any>$('.icon .nav'))
            .popup({
                on: 'click'
            })
        ;
        this.getNotifications()
    }

    getNotifications() {
        this.profileService.getNotifications()
            .subscribe(
                data => {
                    this.notifications = data
                    this.notifications.forEach(notification => this.getUser(notification))
                    this.notifications.sort(this.compare)
                    console.log(this.notifications)
                },
                error => this.handleError(error)
            );
    }

    compare(a, b) {
        if (a.created_at < b.created_at)
            return 1;
        else if (a.created_at > b.created_at)
            return -1;
        else
            return 0;
    }


    getUser(notification) {
        if(notification.follower_id){
            notification.picture = this.service.getURL() + 'users/' + notification.follower_id + '/picture'
        }
    }
    handleError(error) {
        console.log(error)
    }
}