"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var profile_service_1 = require("../../services/profile.service");
var user_1 = require("../../model/user");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var SignupComponent = (function () {
    function SignupComponent(router, profileService, responseHandler, title) {
        this.router = router;
        this.profileService = profileService;
        this.responseHandler = responseHandler;
        this.title = title;
        this.user = new user_1.User();
        title.setTitle("Sign Up");
    }
    SignupComponent.prototype.signup = function () {
        var _this = this;
        if (this.password1 != this.password2) {
            this.responseHandler.errorMessage('The passwords do not match!', 0);
            return;
        }
        if (this.password1.length < 6) {
            this.responseHandler.errorMessage('The password must be at least 6 characters!', 0);
            return;
        }
        this.user.password = this.password1;
        this.profileService.signup(this.user)
            .subscribe(function (data) {
            _this.router.navigate(['Login']);
        }, function (error) { return _this.handleError(error); });
    };
    SignupComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage('The signup was not successful!', error);
    };
    SignupComponent = __decorate([
        core_1.Component({
            selector: 'signup',
            templateUrl: 'app/components/signup/signup.html',
            providers: [profile_service_1.ProfileService]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, profile_service_1.ProfileService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], SignupComponent);
    return SignupComponent;
}());
exports.SignupComponent = SignupComponent;
//# sourceMappingURL=signup.component.js.map