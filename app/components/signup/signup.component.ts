import {Component} from '@angular/core';
import { Router, RouterLink } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import { Http, Headers } from '@angular/http';
import {ProfileService} from "../../services/profile.service";
import {User} from "../../model/user";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'signup',
    templateUrl: 'app/components/signup/signup.html',
    providers: [ProfileService]
})

export class SignupComponent {
    constructor(
        public router:Router,
        private profileService:ProfileService,
        private responseHandler:ResponseHandler,
        private title:Title) {
            title.setTitle("Sign Up")
    }

    user: User = new User();
    password1;
    password2;

    signup() {
        if (this.password1 != this.password2) {
            this.responseHandler.errorMessage('The passwords do not match!',0)
            return
        }
        if(this.password1.length < 6){
            this.responseHandler.errorMessage('The password must be at least 6 characters!',0)
            return
        }

        this.user.password=this.password1;

        this.profileService.signup(this.user)
            .subscribe(
                data => {
                    this.router.navigate(['Login'])
                },
                error => this.handleError(error)
            )
    }

    handleError(error) {
        this.responseHandler.errorMessage('The signup was not successful!',error)
    }
}
