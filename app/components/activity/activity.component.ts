import {Component, OnInit, ElementRef} from   '@angular/core';
import {Router, RouterLink, ROUTER_DIRECTIVES}        from '@angular/router-deprecated';
import {GlobalService} from "../../services/global.service";
import {ProfileService} from "../../services/profile.service";
import {TimeAgoPipe} from "../../TimeAgoPipe";
import {MyDatePipe} from "../../MyDatePipe";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'activity',
    templateUrl: 'app/components/activity/activity.html',
    providers: [ProfileService],
    pipes: [MyDatePipe, TimeAgoPipe],
    directives: [ROUTER_DIRECTIVES]
})

export class ActivityComponent implements OnInit {
    constructor(
        private service:GlobalService,
        private router:Router,
        private profileService:ProfileService,
        private responseHandler:ResponseHandler,
        private title:Title){
            this.title.setTitle("Activity")
        }

    activity;

    ngOnInit() {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return
        }
        this.getActivity()
    }

    getActivity() {
        this.profileService.getActivity()
            .subscribe(
                data => {
                    this.activity = data
                    this.activity.forEach(item => this.getUser(item))
                },
                error => this.handleError(error)
            );
    }


    getUser(item) {
        if (item.user_id) {
            item.picture = this.service.getURL() + 'users/' + item.user_id + '/picture'
        }
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}