"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("../../services/global.service");
var profile_service_1 = require("../../services/profile.service");
var TimeAgoPipe_1 = require("../../TimeAgoPipe");
var MyDatePipe_1 = require("../../MyDatePipe");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var ActivityComponent = (function () {
    function ActivityComponent(service, router, profileService, responseHandler, title) {
        this.service = service;
        this.router = router;
        this.profileService = profileService;
        this.responseHandler = responseHandler;
        this.title = title;
        this.title.setTitle("Activity");
    }
    ActivityComponent.prototype.ngOnInit = function () {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        this.getActivity();
    };
    ActivityComponent.prototype.getActivity = function () {
        var _this = this;
        this.profileService.getActivity()
            .subscribe(function (data) {
            _this.activity = data;
            _this.activity.forEach(function (item) { return _this.getUser(item); });
        }, function (error) { return _this.handleError(error); });
    };
    ActivityComponent.prototype.getUser = function (item) {
        if (item.user_id) {
            item.picture = this.service.getURL() + 'users/' + item.user_id + '/picture';
        }
    };
    ActivityComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    ActivityComponent = __decorate([
        core_1.Component({
            selector: 'activity',
            templateUrl: 'app/components/activity/activity.html',
            providers: [profile_service_1.ProfileService],
            pipes: [MyDatePipe_1.MyDatePipe, TimeAgoPipe_1.TimeAgoPipe],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, router_deprecated_1.Router, profile_service_1.ProfileService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], ActivityComponent);
    return ActivityComponent;
}());
exports.ActivityComponent = ActivityComponent;
//# sourceMappingURL=activity.component.js.map