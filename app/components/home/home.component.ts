import {Component, OnInit} from '@angular/core';
import {ReviewsService} from "../../services/reviews.service";
import {ShowsService} from "../../services/shows.service";
import {GlobalService} from "../../services/global.service";
import {RouterLink, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {TimeAgoPipe} from "../../TimeAgoPipe";
import {MyDatePipe} from "../../MyDatePipe";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'homepage',
    templateUrl: 'app/components/home/home.html',
    providers: [ShowsService, ReviewsService],
    directives: [ROUTER_DIRECTIVES],
    pipes: [MyDatePipe, TimeAgoPipe]
})
export class HomeComponent implements OnInit {

    constructor(
        private showsService:ShowsService,
        private service:GlobalService,
        private reviewsService:ReviewsService,
        private responseHandler:ResponseHandler,
        private title:Title
    ) {
        title.setTitle("TVfan")
    }


    ngOnInit() {
        this.getShows();
        this.getReviews();

        (<any>$('.special.cards .image')).dimmer({
            on: 'hover'
        });
    }

    shows;
    reviews;

    getShows(){
        this.showsService.getRecent()
            .subscribe(
                data => {
                    this.shows = data
                    this.shows.forEach(show => show.poster = this.service.getURL() + 'shows/' + show.id + '/poster')
                },
                error => this.handleError(error)
            );
    }

    getReviews(){
        this.reviewsService.getRecent()
            .subscribe(
                data => {
                    this.reviews = data
                    this.reviews.forEach(review => review.user = this.service.getURL() + 'users/' + review.user_id + '/picture')
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}