"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var reviews_service_1 = require("../../services/reviews.service");
var shows_service_1 = require("../../services/shows.service");
var global_service_1 = require("../../services/global.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var TimeAgoPipe_1 = require("../../TimeAgoPipe");
var MyDatePipe_1 = require("../../MyDatePipe");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var HomeComponent = (function () {
    function HomeComponent(showsService, service, reviewsService, responseHandler, title) {
        this.showsService = showsService;
        this.service = service;
        this.reviewsService = reviewsService;
        this.responseHandler = responseHandler;
        this.title = title;
        title.setTitle("TVfan");
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.getShows();
        this.getReviews();
        $('.special.cards .image').dimmer({
            on: 'hover'
        });
    };
    HomeComponent.prototype.getShows = function () {
        var _this = this;
        this.showsService.getRecent()
            .subscribe(function (data) {
            _this.shows = data;
            _this.shows.forEach(function (show) { return show.poster = _this.service.getURL() + 'shows/' + show.id + '/poster'; });
        }, function (error) { return _this.handleError(error); });
    };
    HomeComponent.prototype.getReviews = function () {
        var _this = this;
        this.reviewsService.getRecent()
            .subscribe(function (data) {
            _this.reviews = data;
            _this.reviews.forEach(function (review) { return review.user = _this.service.getURL() + 'users/' + review.user_id + '/picture'; });
        }, function (error) { return _this.handleError(error); });
    };
    HomeComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'homepage',
            templateUrl: 'app/components/home/home.html',
            providers: [shows_service_1.ShowsService, reviews_service_1.ReviewsService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            pipes: [MyDatePipe_1.MyDatePipe, TimeAgoPipe_1.TimeAgoPipe]
        }), 
        __metadata('design:paramtypes', [shows_service_1.ShowsService, global_service_1.GlobalService, reviews_service_1.ReviewsService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map