import {Component, OnInit, Input} from '@angular/core';
import {GlobalService} from "../../services/global.service";
import {User} from "../../model/user";
import {ProfileService} from "../../services/profile.service";
import {RouteParams, ROUTER_DIRECTIVES, RouterLink, Router} from '@angular/router-deprecated';
import {SMTabs} from "../../semantic/SMTabs";
import {Show} from "../../model/show";
import {FriendsComponent} from "./friends/friends.component";
import {MyDatePipe} from "../../MyDatePipe";
import {ResponseHandler} from "../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'user-profile',
    templateUrl: 'app/components/profile/profile.html',
    providers: [ProfileService],
    pipes: [MyDatePipe],
    directives: [ROUTER_DIRECTIVES, SMTabs, FriendsComponent]
})

export class ProfileComponent implements OnInit {
    constructor(private service:GlobalService,
                private profileService:ProfileService,
                private routeParams:RouteParams,
                private router:Router,
                private responseHandler:ResponseHandler,
                private title:Title){
    }

    user:User;
    watchlist:Show[];
    isMe;
    about;

    ngOnInit() {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return
        }

        let id = this.routeParams.get('id');
        this.getUser(id)
        this.getWatchlist(id)
        this.getAbout(id)
        if (id === this.service.getUser().id) {
            this.isMe = true
        }
    }

    private getUser(id) {
        this.profileService.getUser(id)
            .subscribe(
                data => {
                    this.user = data;
                    this.user.picture = this.service.getURL() + 'users/' + this.user.id + '/picture';
                    this.title.setTitle(this.user.first_name+" "+this.user.last_name)
                },
                error => this.handleError(error)
            );
    }

    getWatchlist(id) {
        this.profileService.getWatchlist(id)
            .subscribe(
                (data) => {
                    this.watchlist = data
                    this.watchlist.forEach(show => show.poster = this.service.getURL() + 'shows/' + show.id + '/poster')
                },
                error => this.handleError(error)
            );
    }


    getAbout(id) {
        this.profileService.getAbout(id)
            .subscribe(
                (data) => {
                    this.about = data
                },
                error => this.handleError(error)
            );
    }


    followUser(id) {
        this.profileService.followUser(id)
            .subscribe(
                data => this.user.following = !this.user.following,
                error => this.handleError(error)
            )
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}