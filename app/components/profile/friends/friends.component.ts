import {Component, OnInit, Input} from '@angular/core';
import {RouteParams, ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {ProfileService} from "../../../services/profile.service";
import {SMTabs} from "../../../semantic/SMTabs";
import {GlobalService} from "../../../services/global.service";
import {User} from "../../../model/user";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'user-friends',
    templateUrl: 'app/components/profile/friends/friends.html',
    providers: [ProfileService],
    directives: [ROUTER_DIRECTIVES, SMTabs]
})

export class FriendsComponent implements OnInit {
    constructor(private service:GlobalService, private profileService:ProfileService,private responseHandler:ResponseHandler) {
    }

    @Input() user_id;
    following:User[];
    followers:User[];

    ngOnInit() {
        this.getFollowing()
        this.getFollowers()
    }

    private getFollowing() {
        this.profileService.getFollowing(this.user_id)
            .subscribe(
                (following) => {
                    this.following = following
                    this.following.forEach(user => user.picture = this.service.getURL() + 'users/' + user.id + '/picture')
                },
                error => this.handleError(error)
            );

    }

    private getFollowers() {
        this.profileService.getFollowers(this.user_id)
            .subscribe(
                (followers) => {
                    this.followers = followers
                    this.followers.forEach(follower => follower.picture = this.service.getURL() + 'users/' + follower.id + '/picture')
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}