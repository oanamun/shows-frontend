import {Component, Input, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {GlobalService} from "../../../services/global.service";
import {User} from "../../../model/user";
import {ProfileService} from "../../../services/profile.service";
import {ResponseHandler} from "../../../services/response_handler.service";
import {Title}                  from '@angular/platform-browser';

@Component({
    selector: 'profile-settings',
    templateUrl: 'app/components/profile/settings/settings.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ProfileService]
})

export class SettingsComponent implements OnInit {
    constructor(private service:GlobalService, private router:Router, private profileService:ProfileService, private responseHandler:ResponseHandler,private title:Title) {
        title.setTitle("Settings")
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login'])
            return
        }
        this.user = this.service.getUser()
        this.pictureUrl = this.service.getURL() + 'users/' + this.user.id + '/picture'
    }

    user:User;
    pictureUrl:string;
    password = {old: "", new1: "", new2: ""}

    ngOnInit() {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return
        }
    }

    update() {
        if(this.user.email.length==0 || this.user.username.length==0){
            this.responseHandler.errorMessage("The required fields are empty!", 0)
            return;
        }

        this.profileService.updateProfile(this.user)
            .subscribe(
                data => {
                    this.responseHandler.successMessage("The changes were saved!")
                    this.service.setUser(data);
                },
                error => this.handleError(error)
            )
    }

    changePassword() {
        if (this.password.new1 != this.password.new2) {
            this.responseHandler.errorMessage("The passwords don't match", 0)
        }
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}