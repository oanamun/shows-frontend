"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("../../../services/global.service");
var profile_service_1 = require("../../../services/profile.service");
var response_handler_service_1 = require("../../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var SettingsComponent = (function () {
    function SettingsComponent(service, router, profileService, responseHandler, title) {
        this.service = service;
        this.router = router;
        this.profileService = profileService;
        this.responseHandler = responseHandler;
        this.title = title;
        this.password = { old: "", new1: "", new2: "" };
        title.setTitle("Settings");
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        this.user = this.service.getUser();
        this.pictureUrl = this.service.getURL() + 'users/' + this.user.id + '/picture';
    }
    SettingsComponent.prototype.ngOnInit = function () {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
    };
    SettingsComponent.prototype.update = function () {
        var _this = this;
        if (this.user.email.length == 0 || this.user.username.length == 0) {
            this.responseHandler.errorMessage("The required fields are empty!", 0);
            return;
        }
        this.profileService.updateProfile(this.user)
            .subscribe(function (data) {
            _this.responseHandler.successMessage("The changes were saved!");
            _this.service.setUser(data);
        }, function (error) { return _this.handleError(error); });
    };
    SettingsComponent.prototype.changePassword = function () {
        if (this.password.new1 != this.password.new2) {
            this.responseHandler.errorMessage("The passwords don't match", 0);
        }
    };
    SettingsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'profile-settings',
            templateUrl: 'app/components/profile/settings/settings.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [profile_service_1.ProfileService]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, router_deprecated_1.Router, profile_service_1.ProfileService, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], SettingsComponent);
    return SettingsComponent;
}());
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=settings.component.js.map