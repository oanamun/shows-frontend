"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var global_service_1 = require("../../services/global.service");
var profile_service_1 = require("../../services/profile.service");
var router_deprecated_1 = require('@angular/router-deprecated');
var SMTabs_1 = require("../../semantic/SMTabs");
var friends_component_1 = require("./friends/friends.component");
var MyDatePipe_1 = require("../../MyDatePipe");
var response_handler_service_1 = require("../../services/response_handler.service");
var platform_browser_1 = require('@angular/platform-browser');
var ProfileComponent = (function () {
    function ProfileComponent(service, profileService, routeParams, router, responseHandler, title) {
        this.service = service;
        this.profileService = profileService;
        this.routeParams = routeParams;
        this.router = router;
        this.responseHandler = responseHandler;
        this.title = title;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        if (!this.service.isLoggedIn()) {
            this.router.navigate(['Login']);
            return;
        }
        var id = this.routeParams.get('id');
        this.getUser(id);
        this.getWatchlist(id);
        this.getAbout(id);
        if (id === this.service.getUser().id) {
            this.isMe = true;
        }
    };
    ProfileComponent.prototype.getUser = function (id) {
        var _this = this;
        this.profileService.getUser(id)
            .subscribe(function (data) {
            _this.user = data;
            _this.user.picture = _this.service.getURL() + 'users/' + _this.user.id + '/picture';
            _this.title.setTitle(_this.user.first_name + " " + _this.user.last_name);
        }, function (error) { return _this.handleError(error); });
    };
    ProfileComponent.prototype.getWatchlist = function (id) {
        var _this = this;
        this.profileService.getWatchlist(id)
            .subscribe(function (data) {
            _this.watchlist = data;
            _this.watchlist.forEach(function (show) { return show.poster = _this.service.getURL() + 'shows/' + show.id + '/poster'; });
        }, function (error) { return _this.handleError(error); });
    };
    ProfileComponent.prototype.getAbout = function (id) {
        var _this = this;
        this.profileService.getAbout(id)
            .subscribe(function (data) {
            _this.about = data;
        }, function (error) { return _this.handleError(error); });
    };
    ProfileComponent.prototype.followUser = function (id) {
        var _this = this;
        this.profileService.followUser(id)
            .subscribe(function (data) { return _this.user.following = !_this.user.following; }, function (error) { return _this.handleError(error); });
    };
    ProfileComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'user-profile',
            templateUrl: 'app/components/profile/profile.html',
            providers: [profile_service_1.ProfileService],
            pipes: [MyDatePipe_1.MyDatePipe],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMTabs_1.SMTabs, friends_component_1.FriendsComponent]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, profile_service_1.ProfileService, router_deprecated_1.RouteParams, router_deprecated_1.Router, response_handler_service_1.ResponseHandler, platform_browser_1.Title])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map