import {Component, OnInit, Input} from '@angular/core';
import {RouteParams, ROUTER_DIRECTIVES, RouterLink} from '@angular/router-deprecated';
import {ProfileService} from "../../../services/profile.service";
import {SMTabs} from "../../../semantic/SMTabs";
import {GlobalService} from "../../../services/global.service";
import {User} from "../../../model/user";
import {MyDatePipe} from "../../../MyDatePipe";
import {ResponseHandler} from "../../../services/response_handler.service";

@Component({
    selector: 'user-reviews',
    templateUrl: 'app/components/profile/reviews/reviews.html',
    providers: [ProfileService],
    directives: [ROUTER_DIRECTIVES, SMTabs],
    pipes: [MyDatePipe]
})

export class ReviewsComponent implements OnInit {
    constructor(private service:GlobalService, private profileService:ProfileService,private responseHandler:ResponseHandler) {
    }

    @Input() user_id;
    reviews;

    ngOnInit() {
        this.getReviews();
    }

    getReviews() {
        this.profileService.getReviews(this.user_id)
            .subscribe(
                data => {
                    this.reviews = data
                },
                error => this.handleError(error)
            );
    }

    handleError(error) {
        this.responseHandler.errorMessage("An errror occured", error)
    }
}