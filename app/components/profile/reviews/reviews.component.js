"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var profile_service_1 = require("../../../services/profile.service");
var SMTabs_1 = require("../../../semantic/SMTabs");
var global_service_1 = require("../../../services/global.service");
var MyDatePipe_1 = require("../../../MyDatePipe");
var response_handler_service_1 = require("../../../services/response_handler.service");
var ReviewsComponent = (function () {
    function ReviewsComponent(service, profileService, responseHandler) {
        this.service = service;
        this.profileService = profileService;
        this.responseHandler = responseHandler;
    }
    ReviewsComponent.prototype.ngOnInit = function () {
        this.getReviews();
    };
    ReviewsComponent.prototype.getReviews = function () {
        var _this = this;
        this.profileService.getReviews(this.user_id)
            .subscribe(function (data) {
            _this.reviews = data;
        }, function (error) { return _this.handleError(error); });
    };
    ReviewsComponent.prototype.handleError = function (error) {
        this.responseHandler.errorMessage("An errror occured", error);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ReviewsComponent.prototype, "user_id", void 0);
    ReviewsComponent = __decorate([
        core_1.Component({
            selector: 'user-reviews',
            templateUrl: 'app/components/profile/reviews/reviews.html',
            providers: [profile_service_1.ProfileService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, SMTabs_1.SMTabs],
            pipes: [MyDatePipe_1.MyDatePipe]
        }), 
        __metadata('design:paramtypes', [global_service_1.GlobalService, profile_service_1.ProfileService, response_handler_service_1.ResponseHandler])
    ], ReviewsComponent);
    return ReviewsComponent;
}());
exports.ReviewsComponent = ReviewsComponent;
//# sourceMappingURL=reviews.component.js.map