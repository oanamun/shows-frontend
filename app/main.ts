import { bootstrap }        from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS}   from '@angular/router-deprecated';
import {HTTP_PROVIDERS}     from '@angular/http';
import {LocalStorage}       from './services/local_storage';
import {ResponseHandler}    from './services/response_handler.service';
import {GlobalService}      from './services/global.service';
import {AppComponent}       from "./components/app/app.component";
import {Title}              from '@angular/platform-browser'
import 'rxjs/Rx';
import 'jquery';
import 'semantic';
import 'trumbowyg';

bootstrap(AppComponent,[HTTP_PROVIDERS,ROUTER_PROVIDERS,Title,LocalStorage,GlobalService, ResponseHandler]);
