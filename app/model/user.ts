import {Serializable} from '../serializable'

export class User extends Serializable{
    id: string;
    username: string;
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    picture: string;
    role_id: string;
    following: boolean;
}