export class Show{
    id:string
    imdb_id: string;
    name: string;
    description: string;
    year: string;
    creator: string;
    channel: string;
    poster: string;
}