import {Directive, Input, OnInit, ElementRef} from '@angular/core'

@Directive({
    selector: "[sm-popup]"
})
export class SMPopup implements OnInit {
    @Input('sm-popup') popup;

    constructor(private el:ElementRef) {
    }

    ngOnInit() {
        (<any>$(this.el.nativeElement)).popup({
            content: this.popup
        });
    }
}