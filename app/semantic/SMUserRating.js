"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var global_service_1 = require("../services/global.service");
var SMUserRating = (function () {
    function SMUserRating(el) {
        this.el = el;
    }
    SMUserRating.prototype.ngOnInit = function () {
        $(this.el.nativeElement).rating({
            initialRating: this.rating || 0,
            maxRating: 5
        });
        $(this.el.nativeElement).rating('setting', 'onRate', function (value) {
            var url = "";
            if (localStorage.getItem("show")) {
                url = 'http://localhost:3333/api/shows/' + localStorage.getItem("show") + '/rate';
                localStorage.removeItem("show");
            }
            else if (localStorage.getItem("episode")) {
                url = 'http://localhost:3333/api/episodes/' + localStorage.getItem("episode") + '/rate';
                localStorage.removeItem("episode");
            }
            $.ajax({
                url: url,
                type: 'post',
                data: {
                    rating: value
                },
                headers: {
                    Token: localStorage.getItem("token")
                },
                dataType: 'json'
            });
        });
    };
    __decorate([
        core_1.Input('sm-urating'), 
        __metadata('design:type', Number)
    ], SMUserRating.prototype, "rating", void 0);
    SMUserRating = __decorate([
        core_1.Directive({
            selector: "[sm-urating]",
            providers: [global_service_1.GlobalService]
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], SMUserRating);
    return SMUserRating;
}());
exports.SMUserRating = SMUserRating;
//# sourceMappingURL=SMUserRating.js.map