import {Directive, Input, OnInit, ElementRef} from '@angular/core'
import {GlobalService} from "../services/global.service";

@Directive({
    selector: "[sm-urating]",
    providers: [GlobalService]
})
export class SMUserRating implements OnInit {
    @Input('sm-urating') rating:number;

    constructor(private el:ElementRef) {
    }

    ngOnInit() {
        (<any>$(this.el.nativeElement)).rating({
            initialRating: this.rating || 0,
            maxRating: 5
        });

        (<any>$(this.el.nativeElement)).rating('setting', 'onRate', function (value) {
            var url = ""
            if (localStorage.getItem("show")) {
                url = 'http://localhost:3333/api/shows/' + localStorage.getItem("show") + '/rate'
                localStorage.removeItem("show");
            }
            else if (localStorage.getItem("episode")) {
                url = 'http://localhost:3333/api/episodes/' + localStorage.getItem("episode") + '/rate'
                localStorage.removeItem("episode");
            }

            $.ajax({
                url: url,
                type: 'post',
                data: {
                    rating: value
                },
                headers: {
                    Token: localStorage.getItem("token")
                },
                dataType: 'json'
            });

        });
    }
}