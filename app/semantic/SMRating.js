"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SMRating = (function () {
    function SMRating(el) {
        this.el = el;
    }
    SMRating.prototype.ngOnInit = function () {
        $(this.el.nativeElement).rating({
            initialRating: this.show_rating || 0,
            maxRating: 5
        });
        $(this.el.nativeElement).rating('disable');
    };
    __decorate([
        core_1.Input('sm-rating'), 
        __metadata('design:type', Number)
    ], SMRating.prototype, "show_rating", void 0);
    SMRating = __decorate([
        core_1.Directive({
            selector: "[sm-rating]"
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], SMRating);
    return SMRating;
}());
exports.SMRating = SMRating;
//# sourceMappingURL=SMRating.js.map