import {Directive, ElementRef, OnInit} from '@angular/core'

@Directive({
    selector: "[sm-tabs]"
})
export class SMTabs {
    constructor(private el: ElementRef) {
    }

    ngOnInit() {
        (<any>jQuery(this.el.nativeElement)).tab();
    }
}