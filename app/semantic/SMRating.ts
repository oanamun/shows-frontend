import {Directive, Input, OnInit, ElementRef} from  '@angular/core'

@Directive({
    selector: "[sm-rating]"
})
export class SMRating implements OnInit {
    @Input('sm-rating') show_rating: number;

    constructor(private el:ElementRef) {
    }

    ngOnInit() {
        (<any>$(this.el.nativeElement)).rating({
            initialRating: this.show_rating || 0,
            maxRating: 5
        });

        (<any>$(this.el.nativeElement)).rating('disable');

    }
}