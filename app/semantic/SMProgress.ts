import {Directive, Input, OnInit, ElementRef} from '@angular/core'

@Directive({
    selector: "[sm-progress]"
})
export class SMProgress implements OnInit {
    @Input('sm-progress') show_progress: number;

    constructor(private el:ElementRef) {
    }

    ngOnInit() {
        (<any>$(this.el.nativeElement)).progress({
            percent: this.show_progress || 0
        });
    }
}