"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var local_storage_1 = require('./services/local_storage');
var response_handler_service_1 = require('./services/response_handler.service');
var global_service_1 = require('./services/global.service');
var app_component_1 = require("./components/app/app.component");
var platform_browser_1 = require('@angular/platform-browser');
require('rxjs/Rx');
require('jquery');
require('semantic');
require('trumbowyg');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [http_1.HTTP_PROVIDERS, router_deprecated_1.ROUTER_PROVIDERS, platform_browser_1.Title, local_storage_1.LocalStorage, global_service_1.GlobalService, response_handler_service_1.ResponseHandler]);
//# sourceMappingURL=main.js.map