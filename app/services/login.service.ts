import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Router}         from '@angular/router-deprecated';
import {Show}           from '../model/show';
import {Observable}     from 'rxjs/Observable';
import {GlobalService}  from './global.service';
import {User} from '../model/user'
import {ResponseHandler} from "./response_handler.service";

@Injectable()
export class LoginService {
    constructor(private http:Http, private router:Router, private service:GlobalService,private responseHandler:ResponseHandler) {
    }

    authenticate(username, password) {
        var creds = "username=" + username + "&password=" + password;

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        this.http.post(this.service.getURL() + 'login', creds, {
                headers: headers
            })
            .map(res => res.json())
            .subscribe(
                (data) => {
                    this.saveJwt(data.token);
                    this.saveUser(data);
                },
                (error) => {
                    if (error.status == 401) {
                        this.responseHandler.errorMessage('Username or password are not correct!', 0)
                    }
                    else {
                        this.responseHandler.errorMessage('Login failed!', 0)
                    }
                },
                () => this.router.navigate(['/Home'])
            );
    }

    saveJwt(jwt) {
        if (jwt) {
            this.service.setToken(jwt)
        }
    }

    saveUser(user:User) {
        if (user) {
            this.service.setUser(user)
        }
    }
}