import {Injectable}     from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {GlobalService} from "./global.service";

@Injectable()
export class ResponseHandler {
    constructor(private router:Router, private service: GlobalService) {
    }

    successMessage(message: string){
        this.showMessage(message)
        document.getElementById("status-message").classList.add("success", "info-message-activate");
    }

    errorMessage(message, error){
        if (error == 401) {
            this.service.logout()
            this.router.navigate(['Login'])
            return
        }
        let mes = (error==0) ? message :  message+ " ("+error+")"
        this.showMessage(mes)
        document.getElementById("status-message").classList.add("error", "info-message-activate");
    }

    showMessage(message){
        const status_message = document.getElementById("status-message")
        status_message.innerHTML = message;
        status_message.classList.remove("success", "error", "info-message-activate");

        const new_elem = status_message.cloneNode(true);
        status_message.parentNode.replaceChild(new_elem, status_message);
        //document.getElementById("status-message").offsetWidth =document.getElementById("status-message").offsetWidth;
    }

}