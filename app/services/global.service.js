"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var local_storage_1 = require('./local_storage');
var user_1 = require('../model/user');
var GlobalService = (function () {
    function GlobalService(_localStorage) {
        this._localStorage = _localStorage;
    }
    GlobalService.prototype.getURL = function () {
        return 'http://localhost:3333/api/';
    };
    GlobalService.prototype.getToken = function () {
        return this._localStorage.get('token');
    };
    GlobalService.prototype.setToken = function (token) {
        this._localStorage.set('token', token);
    };
    GlobalService.prototype.getUserFromStorage = function () {
        this.user = new user_1.User();
        if (this._localStorage.get('user')) {
            this.user.fillFromJSON(this._localStorage.get('user'));
            return this.user;
        }
        else {
            return null;
        }
    };
    GlobalService.prototype.getUser = function () {
        if (!this.user) {
            this.getUserFromStorage();
        }
        return this.user;
    };
    GlobalService.prototype.isLoggedIn = function () {
        if (this._localStorage.get('user')) {
            return true;
        }
        return false;
    };
    GlobalService.prototype.setUser = function (user) {
        this._localStorage.set('user', JSON.stringify(user));
        this.user = user;
    };
    GlobalService.prototype.logout = function () {
        if (this.isLoggedIn()) {
            this._localStorage.remove('token');
            this._localStorage.remove('user');
        }
    };
    GlobalService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [local_storage_1.LocalStorage])
    ], GlobalService);
    return GlobalService;
}());
exports.GlobalService = GlobalService;
//# sourceMappingURL=global.service.js.map