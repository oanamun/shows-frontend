"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_deprecated_1 = require('@angular/router-deprecated');
var Observable_1 = require('rxjs/Observable');
var global_service_1 = require('./global.service');
var ProfileService = (function () {
    function ProfileService(http, service, router) {
        this.http = http;
        this.service = service;
        this.router = router;
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new http_1.RequestOptions({ headers: headers });
    }
    ProfileService.prototype.getUser = function (id) {
        return this.http.get(this.service.getURL() + 'user/' + id, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getFollowing = function (id) {
        return this.http.get(this.service.getURL() + 'users/' + id + '/following', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getFollowers = function (id) {
        return this.http.get(this.service.getURL() + 'users/' + id + '/followers', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getWatchlist = function (id) {
        return this.http.get(this.service.getURL() + 'users/' + id + '/watching', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.followUser = function (id) {
        var body = JSON.stringify({ "friend_id": id });
        return this.http.post(this.service.getURL() + 'users/follow', body, this.options)
            .map(function (response) { return response; })
            .catch(this.handleError);
    };
    ProfileService.prototype.getReviews = function (id) {
        return this.http.get(this.service.getURL() + 'users/' + id + '/reviews', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getNotifications = function () {
        return this.http.get(this.service.getURL() + 'users/notifications', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getActivity = function () {
        return this.http.get(this.service.getURL() + 'users/activity', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.getAbout = function (id) {
        return this.http.get(this.service.getURL() + 'users/' + id + '/about', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.updateProfile = function (user) {
        var body = JSON.stringify({ "username": user.username, "email": user.email, "first_name": user.first_name, "last_name": user.last_name });
        return this.http.put(this.service.getURL() + 'users/' + user.id, body, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.signup = function (user) {
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
        });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify({ "username": user.username, "password": user.password, "email": user.email, "first_name": user.first_name, "last_name": user.last_name, "role_id": 3 });
        return this.http.post(this.service.getURL() + 'users', body, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.status);
    };
    ProfileService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, global_service_1.GlobalService, router_deprecated_1.Router])
    ], ProfileService);
    return ProfileService;
}());
exports.ProfileService = ProfileService;
//# sourceMappingURL=profile.service.js.map