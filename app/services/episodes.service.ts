import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {LocalStorage}   from './local_storage';
import {GlobalService}  from './global.service';
import {Show}           from '../model/show'
import {User} from "../model/user";

@Injectable()
export class EpisodesService {
    constructor(private http:Http, private service:GlobalService) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new RequestOptions({headers: headers});
    }

    options;

    getEpisodes(season_id) {
        return this.http.get(this.service.getURL() + 'seasons/'+season_id+'/episodes')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getEpisode(episode_id){
        return this.http.get(this.service.getURL() + 'episodes/'+episode_id)
            .map(response => response.json())
            .catch(this.handleError);
    }


    getEpisodeWatchers(episode_id){
        return this.http.get(this.service.getURL() + 'episodes/'+episode_id+'/watchers', this.options)
            .map(response => <User[]>response.json())
            .catch(this.handleError);
    }


    getDiscussions(episode_id){
        return this.http.get(this.service.getURL() + 'episodes/'+episode_id+'/discussions')
            .map(response => response.json())
            .catch(this.handleError);
    }

    watchEpisode(episode_id){
        let body = JSON.stringify({"episode_id" : episode_id});
        return this.http.post(this.service.getURL() + 'users/see', body, this.options)
            .map(response => response)
            .catch(this.handleError)
    }

    addDiscussion(title, episode_id){
        let body = JSON.stringify({"title": title, "episode_id": episode_id});
        return this.http.post(this.service.getURL() + 'discussions', body, this.options)
            .map(response => response.json())
            .catch(this.handleError)
    }

    isWatching(episode_id){
        return this.http.get(this.service.getURL() + 'users/isWatching?episode_id='+episode_id, this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }


    getEpisodeRating(id) {
        return this.http.get(this.service.getURL() + 'users/myRating?episode_id=' + id, this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }


    private handleError(error:Response) {
        return Observable.throw(error.status);
    }
}