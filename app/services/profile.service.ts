import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Router, RouterLink, ROUTER_DIRECTIVES}        from '@angular/router-deprecated';
import {Observable}     from 'rxjs/Observable';
import {LocalStorage}   from './local_storage';
import {GlobalService}  from './global.service';
import {User}           from '../model/user'
import {Show} from "../model/show";

@Injectable()
export class ProfileService {
    constructor(private http:Http, private service:GlobalService, private router: Router) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new RequestOptions({headers: headers});
    }
    options: RequestOptions;

    getUser(id){
        return this.http.get(this.service.getURL() + 'user/'+ id, this.options)
            .map(response => <User>response.json())
            .catch(this.handleError);
    }

    getFollowing(id) {
        return this.http.get(this.service.getURL() + 'users/'+id+'/following', this.options)
            .map(response => <User[]> response.json())
            .catch(this.handleError);
    }

    getFollowers(id) {
        return this.http.get(this.service.getURL() + 'users/'+id+'/followers', this. options)
            .map(response => <User[]> response.json())
            .catch(this.handleError);
    }

    getWatchlist(id){
        return this.http.get(this.service.getURL() + 'users/'+id+'/watching', this.options)
            .map(response => <Show[]> response.json())
            .catch(this.handleError);
    }

    followUser(id){
        let body = JSON.stringify({"friend_id" : id});
        return this.http.post(this.service.getURL() + 'users/follow', body, this.options)
            .map(response => response)
            .catch(this.handleError)
    }

    getReviews(id: string){
        return this.http.get(this.service.getURL() + 'users/'+id+'/reviews', this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getNotifications(){
        return this.http.get(this.service.getURL() + 'users/notifications', this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getActivity(){
        return this.http.get(this.service.getURL() + 'users/activity', this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getAbout(id){
        return this.http.get(this.service.getURL() + 'users/'+id+'/about', this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    updateProfile(user: User){
        let body = JSON.stringify({"username": user.username, "email":user.email, "first_name":user.first_name, "last_name":user.last_name});
        return this.http.put(this.service.getURL() + 'users/'+user.id, body, this.options)
            .map(response => <User>response.json())
            .catch(this.handleError)
    }

    signup(user: User){
        let headers = new Headers({
            'Content-Type': 'application/json',
        });
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify({"username": user.username, "password":user.password, "email":user.email, "first_name":user.first_name, "last_name":user.last_name, "role_id":3});
        return this.http.post(this.service.getURL() + 'users', body, options)
            .map(response => <User>response.json())
            .catch(this.handleError)
    }

    private handleError(error:Response) {
        return Observable.throw(error.status);
    }
}