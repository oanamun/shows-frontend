import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {LocalStorage}   from './local_storage';
import {GlobalService}  from './global.service';

@Injectable()
export class ReviewsService {
    constructor(private http:Http, private service:GlobalService) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new RequestOptions({headers: headers});
    }

    options;

    getReview(review_id) {
        return this.http.get(this.service.getURL() + 'reviews/'+review_id)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getRecent() {
        return this.http.get(this.service.getURL() + 'reviews/recent')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getComments(review_id){
        return this.http.get(this.service.getURL() + 'reviews/'+review_id+'/comments')
            .map(response => response.json())
            .catch(this.handleError);
    }

    addComment(message, review_id){
        let body = JSON.stringify({"message": message, "review_id": review_id});
        return this.http.post(this.service.getURL() + 'rcomments', body, this.options)
            .map(response => response.json())
            .catch(this.handleError)
    }

    private handleError(error:Response) {
        return Observable.throw(error.status);
    }
}