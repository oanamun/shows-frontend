import {Injectable} from '@angular/core';
import {LocalStorage} from './local_storage';
import {User} from '../model/user'

@Injectable()
export class GlobalService {
    constructor(private _localStorage:LocalStorage) {
    }

    user: User;

    getURL() {
        return 'http://localhost:3333/api/';
    }

    getToken() {
        return this._localStorage.get('token')
    }

    setToken(token:string) {
        this._localStorage.set('token', token)
    }

    getUserFromStorage() {
        this.user = new User();
        if(this._localStorage.get('user')) {
            this.user.fillFromJSON(this._localStorage.get('user'))
            return this.user
        }
        else{
            return null
        }
    }

    getUser(){
        if(!this.user){
            this.getUserFromStorage()
        }
        return this.user
    }

    isLoggedIn(){
        if(this._localStorage.get('user')) {
            return true
        }
        return false
    }

    setUser(user:User) {
        this._localStorage.set('user', JSON.stringify(user))
        this.user=user;
    }

    logout(){
        if(this.isLoggedIn()){
            this._localStorage.remove('token')
            this._localStorage.remove('user')
        }
    }
}
