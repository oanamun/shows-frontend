"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require("./global.service");
var ResponseHandler = (function () {
    function ResponseHandler(router, service) {
        this.router = router;
        this.service = service;
    }
    ResponseHandler.prototype.successMessage = function (message) {
        this.showMessage(message);
        document.getElementById("status-message").classList.add("success", "info-message-activate");
    };
    ResponseHandler.prototype.errorMessage = function (message, error) {
        if (error == 401) {
            this.service.logout();
            this.router.navigate(['Login']);
            return;
        }
        var mes = (error == 0) ? message : message + " (" + error + ")";
        this.showMessage(mes);
        document.getElementById("status-message").classList.add("error", "info-message-activate");
    };
    ResponseHandler.prototype.showMessage = function (message) {
        var status_message = document.getElementById("status-message");
        status_message.innerHTML = message;
        status_message.classList.remove("success", "error", "info-message-activate");
        var new_elem = status_message.cloneNode(true);
        status_message.parentNode.replaceChild(new_elem, status_message);
        //document.getElementById("status-message").offsetWidth =document.getElementById("status-message").offsetWidth;
    };
    ResponseHandler = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, global_service_1.GlobalService])
    ], ResponseHandler);
    return ResponseHandler;
}());
exports.ResponseHandler = ResponseHandler;
//# sourceMappingURL=response_handler.service.js.map