"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var global_service_1 = require('./global.service');
var EpisodesService = (function () {
    function EpisodesService(http, service) {
        this.http = http;
        this.service = service;
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new http_1.RequestOptions({ headers: headers });
    }
    EpisodesService.prototype.getEpisodes = function (season_id) {
        return this.http.get(this.service.getURL() + 'seasons/' + season_id + '/episodes')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.getEpisode = function (episode_id) {
        return this.http.get(this.service.getURL() + 'episodes/' + episode_id)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.getEpisodeWatchers = function (episode_id) {
        return this.http.get(this.service.getURL() + 'episodes/' + episode_id + '/watchers', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.getDiscussions = function (episode_id) {
        return this.http.get(this.service.getURL() + 'episodes/' + episode_id + '/discussions')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.watchEpisode = function (episode_id) {
        var body = JSON.stringify({ "episode_id": episode_id });
        return this.http.post(this.service.getURL() + 'users/see', body, this.options)
            .map(function (response) { return response; })
            .catch(this.handleError);
    };
    EpisodesService.prototype.addDiscussion = function (title, episode_id) {
        var body = JSON.stringify({ "title": title, "episode_id": episode_id });
        return this.http.post(this.service.getURL() + 'discussions', body, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.isWatching = function (episode_id) {
        return this.http.get(this.service.getURL() + 'users/isWatching?episode_id=' + episode_id, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.getEpisodeRating = function (id) {
        return this.http.get(this.service.getURL() + 'users/myRating?episode_id=' + id, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EpisodesService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.status);
    };
    EpisodesService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, global_service_1.GlobalService])
    ], EpisodesService);
    return EpisodesService;
}());
exports.EpisodesService = EpisodesService;
//# sourceMappingURL=episodes.service.js.map