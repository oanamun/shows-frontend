"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var global_service_1 = require('./global.service');
var ShowsService = (function () {
    function ShowsService(http, service) {
        this.http = http;
        this.service = service;
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new http_1.RequestOptions({ headers: headers });
    }
    ShowsService.prototype.getShows = function (type) {
        switch (type) {
            case 'all':
                return this.http.get(this.service.getURL() + 'shows')
                    .map(function (response) { return response.json(); })
                    .catch(this.handleError);
            case 'watchlist':
                return this.http.get(this.service.getURL() + 'users/watching', this.options)
                    .map(function (response) { return response.json(); })
                    .catch(this.handleError);
            default:
                return this.http.get(this.service.getURL() + 'categories/' + type + '/shows')
                    .map(function (response) { return response.json(); })
                    .catch(this.handleError);
        }
    };
    ShowsService.prototype.getRecent = function () {
        return this.http.get(this.service.getURL() + 'shows/recent')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getCategories = function () {
        return this.http.get(this.service.getURL() + 'categories')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getShow = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getShowCategories = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/categories')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getShowWatchers = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/watchers', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getSeasons = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/seasons')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getReviews = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/reviews')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getShowRating = function (id) {
        return this.http.get(this.service.getURL() + 'users/myRating?show_id=' + id, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.getWatchPercent = function (id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/percent', this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.watchShow = function (id) {
        var body = JSON.stringify({ "show_id": id });
        return this.http.post(this.service.getURL() + 'users/watch', body, this.options)
            .map(function (response) { return response; })
            .catch(this.handleError);
    };
    ShowsService.prototype.rateShow = function (id, rating) {
        var body = JSON.stringify({ "rating": rating });
        return this.http.post(this.service.getURL() + 'shows/' + id + '/rate', body, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.reviewShow = function (id, review) {
        var body = JSON.stringify({ "title": review.title, "message": review.message, show_id: id });
        return this.http.post(this.service.getURL() + 'reviews', body, this.options)
            .map(function (response) { return response; })
            .catch(this.handleError);
    };
    ShowsService.prototype.recommendShow = function (show_id, friend_id) {
        var body = JSON.stringify({ "friend_id": friend_id, "show_id": show_id });
        return this.http.post(this.service.getURL() + 'shows/recommend', body, this.options)
            .map(function (response) { return response; })
            .catch(this.handleError);
    };
    ShowsService.prototype.addShow = function (show) {
        var body = JSON.stringify(show);
        return this.http.post(this.service.getURL() + 'shows', body, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.isWatching = function (id) {
        return this.http.get(this.service.getURL() + 'users/isWatching?show_id=' + id, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ShowsService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.status);
    };
    ShowsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, global_service_1.GlobalService])
    ], ShowsService);
    return ShowsService;
}());
exports.ShowsService = ShowsService;
//# sourceMappingURL=shows.service.js.map