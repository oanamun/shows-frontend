import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {LocalStorage}   from './local_storage';
import {GlobalService}  from './global.service';
import {Show}           from '../model/show'
import {User}           from "../model/user";

@Injectable()
export class ShowsService {
    constructor(private http:Http, private service:GlobalService) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new RequestOptions({headers: headers});
    }

    options;

    getShows(type:string) {
        switch (type) {
            case 'all':
                return this.http.get(this.service.getURL() + 'shows')
                    .map(response => <Show[]> response.json())
                    .catch(this.handleError);
            case 'watchlist':
                return this.http.get(this.service.getURL() + 'users/watching', this.options)
                    .map(response => <Show[]> response.json())
                    .catch(this.handleError);
            default:
                return this.http.get(this.service.getURL() + 'categories/' + type + '/shows')
                    .map(response => <Show[]> response.json())
                    .catch(this.handleError);
        }
    }

    getRecent() {
        return this.http.get(this.service.getURL() + 'shows/recent')
            .map(response => <Show[]> response.json())
            .catch(this.handleError);
    }

    getCategories() {
        return this.http.get(this.service.getURL() + 'categories')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getShow(id:any) {
        return this.http.get(this.service.getURL() + 'shows/' + id)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getShowCategories(id:any) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/categories')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getShowWatchers(id:any) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/watchers', this.options)
            .map(response => <User[]>response.json())
            .catch(this.handleError);
    }

    getSeasons(id:any) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/seasons')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getReviews(id:any) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/reviews')
            .map(response => response.json())
            .catch(this.handleError);
    }

    getShowRating(id) {
        return this.http.get(this.service.getURL() + 'users/myRating?show_id=' + id, this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getWatchPercent(id) {
        return this.http.get(this.service.getURL() + 'shows/' + id + '/percent', this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }

    watchShow(id) {
        let body = JSON.stringify({"show_id": id});
        return this.http.post(this.service.getURL() + 'users/watch', body, this.options)
            .map(response => response)
            .catch(this.handleError)
    }

    rateShow(id, rating) {
        let body = JSON.stringify({"rating": rating});
        return this.http.post(this.service.getURL() + 'shows/' + id + '/rate', body, this.options)
            .map(response => response.json())
            .catch(this.handleError)
    }

    reviewShow(id, review) {
        let body = JSON.stringify({"title": review.title, "message": review.message, show_id: id});
        return this.http.post(this.service.getURL() + 'reviews', body, this.options)
            .map(response => response)
            .catch(this.handleError)
    }

    recommendShow(show_id, friend_id) {
        let body = JSON.stringify({"friend_id": friend_id, "show_id": show_id});
        return this.http.post(this.service.getURL() + 'shows/recommend', body, this.options)
            .map(response => response)
            .catch(this.handleError)
    }

    addShow(show:Show) {
        let body = JSON.stringify(show);
        return this.http.post(this.service.getURL() + 'shows', body, this.options)
            .map(response => <Show> response.json())
            .catch(this.handleError)
    }

    isWatching(id) {
        return this.http.get(this.service.getURL() + 'users/isWatching?show_id=' + id, this.options)
            .map(response => response.json())
            .catch(this.handleError);
    }


    private handleError(error:Response) {
        return Observable.throw(error.status);
    }
}