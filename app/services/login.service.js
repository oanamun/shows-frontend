"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_deprecated_1 = require('@angular/router-deprecated');
var global_service_1 = require('./global.service');
var response_handler_service_1 = require("./response_handler.service");
var LoginService = (function () {
    function LoginService(http, router, service, responseHandler) {
        this.http = http;
        this.router = router;
        this.service = service;
        this.responseHandler = responseHandler;
    }
    LoginService.prototype.authenticate = function (username, password) {
        var _this = this;
        var creds = "username=" + username + "&password=" + password;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.http.post(this.service.getURL() + 'login', creds, {
            headers: headers
        })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.saveJwt(data.token);
            _this.saveUser(data);
        }, function (error) {
            if (error.status == 401) {
                _this.responseHandler.errorMessage('Username or password are not correct!', 0);
            }
            else {
                _this.responseHandler.errorMessage('Login failed!', 0);
            }
        }, function () { return _this.router.navigate(['/Home']); });
    };
    LoginService.prototype.saveJwt = function (jwt) {
        if (jwt) {
            this.service.setToken(jwt);
        }
    };
    LoginService.prototype.saveUser = function (user) {
        if (user) {
            this.service.setUser(user);
        }
    };
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_deprecated_1.Router, global_service_1.GlobalService, response_handler_service_1.ResponseHandler])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map