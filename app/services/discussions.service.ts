import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {LocalStorage}   from './local_storage';
import {GlobalService}  from './global.service';

@Injectable()
export class DiscussionsService {
    constructor(private http:Http, private service:GlobalService) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new RequestOptions({headers: headers});
    }

    options;

    getDiscussion(discussion_id) {
        return this.http.get(this.service.getURL() + 'discussions/'+discussion_id)
            .map(response => response.json())
            .catch(this.handleError);
    }

    getComments(discussion_id){
        return this.http.get(this.service.getURL() + 'discussions/'+discussion_id+'/comments')
            .map(response => response.json())
            .catch(this.handleError);
    }


    addComment(message, discussion_id){
        let body = JSON.stringify({"message": message, "discussion_id": discussion_id});
        return this.http.post(this.service.getURL() + 'dcomments', body, this.options)
            .map(response => response.json())
            .catch(this.handleError)
    }

    private handleError(error:Response) {
        return Observable.throw(error.status);

    }
}