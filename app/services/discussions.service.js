"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var global_service_1 = require('./global.service');
var DiscussionsService = (function () {
    function DiscussionsService(http, service) {
        this.http = http;
        this.service = service;
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
            'Token': this.service.getToken()
        });
        this.options = new http_1.RequestOptions({ headers: headers });
    }
    DiscussionsService.prototype.getDiscussion = function (discussion_id) {
        return this.http.get(this.service.getURL() + 'discussions/' + discussion_id)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DiscussionsService.prototype.getComments = function (discussion_id) {
        return this.http.get(this.service.getURL() + 'discussions/' + discussion_id + '/comments')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DiscussionsService.prototype.addComment = function (message, discussion_id) {
        var body = JSON.stringify({ "message": message, "discussion_id": discussion_id });
        return this.http.post(this.service.getURL() + 'dcomments', body, this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DiscussionsService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.status);
    };
    DiscussionsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, global_service_1.GlobalService])
    ], DiscussionsService);
    return DiscussionsService;
}());
exports.DiscussionsService = DiscussionsService;
//# sourceMappingURL=discussions.service.js.map