import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'mydate'})
export class MyDatePipe implements PipeTransform {
    transform(timestamp): Date {
        var str = timestamp.split(".")
        var date = new Date(str[0])
        return date;
    }
}