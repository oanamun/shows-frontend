"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var TimeAgoPipe = (function () {
    function TimeAgoPipe() {
    }
    TimeAgoPipe.prototype.transform = function (date) {
        var result;
        // current time
        var d = new Date();
        var now = d.getTime() + (d.getTimezoneOffset() * 60000);
        // time since message was sent in seconds
        var delta = (now - date.getTime()) / 1000;
        // format string
        if (delta < 10) {
            result = 'Now';
        }
        else if (delta < 60) {
            result = Math.floor(delta) + ' seconds ago';
        }
        else if (delta < 3600) {
            result = Math.floor(delta / 60) + ' minutes ago';
        }
        else if (delta < 86400) {
            result = Math.floor(delta / 3600) + ' hours ago';
        }
        else if (delta < 2592000) {
            result = Math.floor(delta / 86400) + ' days ago';
        }
        else {
            result = Math.floor(delta / 2592000) + ' months ago';
        }
        return result;
    };
    TimeAgoPipe = __decorate([
        core_1.Pipe({
            name: 'timeAgo'
        }), 
        __metadata('design:paramtypes', [])
    ], TimeAgoPipe);
    return TimeAgoPipe;
}());
exports.TimeAgoPipe = TimeAgoPipe;
//# sourceMappingURL=TimeAgoPipe.js.map