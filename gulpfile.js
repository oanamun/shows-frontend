var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csslint = require('gulp-csslint');
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var imagemin = require("gulp-imagemin");
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp
        .src('resources/style/style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('build'));
});

gulp.task('img', function () {
    return gulp.src('resources/images/*')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('build/images'));
});

gulp.task('watch', function () {
    gulp.watch('resources/style/*.scss', ['sass']);
    gulp.watch('resources/images/**', ['img']);
});

gulp.task('default', ['sass', 'img', 'watch']);